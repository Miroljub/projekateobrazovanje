import { Component, OnInit, Input } from '@angular/core';
import { NastavnikService } from '../nastavnici/nastavnik.service';
import { UceniciService } from '../ucenici/ucenici.service';
import { PredmetService } from '../predmeti/predmet.service';
import { Ucenik } from '../ucenici/ucenici.model';
import { Predmet } from '../predmeti/predmet.model';
import { Nastavnik } from '../nastavnici/nastavnik.model';
import { AuthService } from '../servies/authService.service';
import { Router } from '@angular/router';
import { rootRoute } from '@angular/router/src/router_module';
import { identifierModuleUrl } from '@angular/compiler';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  role:string;
  url:string = 'http://localhost:4200/';
 
  id:any;


  constructor(private nastavniciService: NastavnikService, private uceniciServis: UceniciService,
               private predmetService: PredmetService,private authServis:AuthService,private router:Router) { }

  ngOnInit() {
    
    this.role = localStorage.getItem('role')
    if (this.role !== 'ADMIN'){
      this.id = localStorage.getItem('id');
    }
    console.log("1")
    this.uceniciServis.getUceniciBaza().subscribe(
      (ucenici: Ucenik[]) => {      
        this.uceniciServis.setUcenik(ucenici)
      });  
     console.log(localStorage.getItem('token'))

      this.predmetService.getPredmetiBaza().subscribe(
        (predmeti: Predmet[]) => {
          this.predmetService.setPredmet(predmeti)
        });

        this.nastavniciService.getNastavniciBaza().subscribe(
          (nastavnici: Nastavnik[]) => {
            this.nastavniciService.setNastavnik(nastavnici)
          });

  }

  logOut(){
    localStorage.removeItem('token');
    localStorage.removeItem('role');
    this.authServis.logOut();
    this.router.navigate(['']);
  }
  vracaj(){
    this.router.navigate(['']);
  }

}
