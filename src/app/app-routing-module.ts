import { NgModule } from '@angular/core';
import {Routes, RouterModule} from '@angular/router'
import { UceniciComponent } from './ucenici/ucenici.component';
import { AppComponent } from './app.component';
import { NastavniciComponent } from './nastavnici/nastavnici.component';
import { PredmetiComponent } from './predmeti/predmeti.component';
import { UceniciStartComponent } from './ucenici/ucenici-start/ucenici-start.component';
import { UceniciDetaljiComponent } from './ucenici/ucenici-detalji/ucenici-detalji.component';
import { UceniciEditComponent } from './ucenici/ucenici-edit/ucenici-edit.component';
import { NastavniciStartComponent } from './nastavnici/nastavnici-start/nastavnici-start.component';
import { NastavniciEditComponent } from './nastavnici/nastavnici-edit/nastavnici-edit.component';
import { NastavniciDetaljiComponent } from './nastavnici/nastavnici-detalji/nastavnici-detalji.component';
import { PredmetiEditComponent } from './predmeti/predmeti-edit/predmeti-edit.component';
import { PredmetiDetaljiComponent } from './predmeti/predmeti-detalji/predmeti-detalji.component';
import { PredmetiStartComponent } from './predmeti/predmeti-start/predmeti-start.component';
import { PredmetiPohadjaComponent } from './predmeti/predmeti-pohadja/predmeti-pohadja.component';
import { PredmetiPredajeComponent } from './predmeti/predmeti-predaje/predmeti-predaje.component';
import { HomeComponent } from './home/home.component';
import { UcenikLicniPodaciComponent } from './ucenici/ucenik-licni-podaci/ucenik-licni-podaci.component';
import { UcenikPodaciEditComponent } from './ucenici/ucenik-licni-podaci/ucenik-podaci-edit/ucenik-podaci-edit.component';
import { UcenikUplatnicaComponent } from './uplate/ucenik-uplatnica/ucenik-uplatnica.component';
import { NastavnikPanelComponent } from './nastavnik-panel/nastavnik-panel.component';
import { UcenikComponent } from './ucenik/ucenik.component';
import { PredmetKojiPohadjaDetaljiComponent } from './ucenik/predmet-koji-pohadja-detalji/predmet-koji-pohadja-detalji.component';
import { PredmetNastavnikDetaljiComponent } from './nastavnik-panel/predmet-nastavnik-detalji/predmet-nastavnik-detalji.component';
import { RokComponent } from './nastavnik-panel/rok/rok.component';
import { OceniRokComponent } from './nastavnik-panel/oceni-rok/oceni-rok.component';
import { PolozeniPredmetiComponent } from './ucenik/polozeni-predmeti/polozeni-predmeti.component';
import { NepolozeniPredmetiComponent } from './ucenik/nepolozeni-predmeti/nepolozeni-predmeti.component';
import { OceniUcenikeComponent } from './nastavnik-panel/oceni-ucenike/oceni-ucenike.component';
import { DavanjeOceneComponent } from './nastavnik-panel/davanje-ocene/davanje-ocene.component';
import { PrijavaIspitaComponent } from './ucenik/prijava-ispita/prijava-ispita.component';
import { ListaNepolozenihPredmetaComponent } from './ucenik/prijava-ispita/lista-nepolozenih-predmeta/lista-nepolozenih-predmeta.component';
import { PrijaviIspitComponent } from './ucenik/prijava-ispita/prijavi-ispit/prijavi-ispit.component';
import { IstorijaUplataComponent } from './ucenik/istorija-uplata/istorija-uplata.component';
import { NastavniciPasswordComponent } from './nastavnici/nastavnici-password/nastavnici-password.component';


const appRoutes: Routes =[
    {path: '', component:HomeComponent},
    {path: 'ucenici', component: UceniciComponent, children: [
        { path: '', component: UceniciStartComponent },
        { path: 'new', component: UceniciEditComponent },
        { path: ':id', component: UceniciDetaljiComponent },
        { path: ':id/edit', component: UceniciEditComponent }
    ]},
    {path: 'nastavnici', component: NastavniciComponent, children:[
        {path: '',component: NastavniciStartComponent},
        {path: 'new', component: NastavniciEditComponent},
        {path: ':id', component: NastavniciDetaljiComponent},
        {path: ':id/edit', component: NastavniciEditComponent},
        {path: ':id/promenaPassword', component: NastavniciPasswordComponent}
    ]},
    {path: 'nastavnik/:id', component: NastavniciDetaljiComponent},
    {path: 'nastavnik/:id/edit', component: NastavniciEditComponent},
    {path: 'nastavnik/:id/promenaPassword', component: NastavniciPasswordComponent},



    {path: 'predmeti', component: PredmetiComponent, children: [
        { path: '', component: PredmetiStartComponent },
        { path: 'new', component: PredmetiEditComponent },
        { path: ':id', component: PredmetiDetaljiComponent },
        { path: ':id/edit', component: PredmetiEditComponent },
        { path: ':id/pohadja', component: PredmetiPohadjaComponent },
        { path: ':id/predaje', component: PredmetiPredajeComponent }
    ]},

    {path: 'nastavnik_predmeti', component: NastavnikPanelComponent,children:[
        {path: ':id',component:PredmetNastavnikDetaljiComponent},
        {path: ':id/ispiti/:idI',component:OceniRokComponent},
        { path: ':id/rok', component: RokComponent },
        { path: ':id/ucenici', component: OceniUcenikeComponent },
        { path: ':id/ucenici/oceni/:idU', component: DavanjeOceneComponent }

    ]},

    { path: 'ucenik/:id', component: UcenikLicniPodaciComponent },
    { path: 'ucenik/:id/edit', component: UcenikPodaciEditComponent },
    { path: 'uplata/:id', component: UcenikUplatnicaComponent },
    { path: 'sviPredmeti', component: UcenikComponent, children: [
        { path: ':id', component: PredmetKojiPohadjaDetaljiComponent }      
    ] },
    { path: 'polozeniPredmeti', component: PolozeniPredmetiComponent },
    { path: 'nepolozeniPredmeti', component: NepolozeniPredmetiComponent },
    { path: 'prijavaIspita', component: PrijavaIspitaComponent, children:[
        { path: ':id', component: PrijaviIspitComponent }
    ] },
    { path: 'istorijaUplata', component: IstorijaUplataComponent }
] 

@NgModule({
    imports:[RouterModule.forRoot(appRoutes)],
    exports: [RouterModule]
})

export class AppRoutingModule{}