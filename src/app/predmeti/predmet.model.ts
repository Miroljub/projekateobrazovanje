import { Pohadja } from '../models/pohadja.model';
import { Predaje } from '../models/predaje.model';

export class Predmet{
    constructor(
        public id:number,
        public naziv:string,
        public espb:number,
        public pohadja:Pohadja[],
        public predaje:Predaje[]
    ){}
}