import { Component, OnInit, Input } from '@angular/core';
import { Predmet } from '../../predmet.model';

@Component({
  selector: 'app-predmet',
  templateUrl: './predmet.component.html',
  styleUrls: ['./predmet.component.css']
})
export class PredmetComponent implements OnInit {

  @Input() predmet: Predmet;
  @Input() index: number;

  constructor() { }

  ngOnInit() {
  }

}
