import { Component, OnInit } from '@angular/core';
import { Predmet } from '../predmet.model';
import { PredmetService } from '../predmet.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-predmeti-lista',
  templateUrl: './predmeti-lista.component.html',
  styleUrls: ['./predmeti-lista.component.css']
})
export class PredmetiListaComponent implements OnInit {

  predmeti: Predmet[]=[];

  constructor(private predmetService:PredmetService, private router: Router, private route: ActivatedRoute) { }

  ngOnInit() {
    this.predmetService.predmetChanged
    .subscribe(
      (predmeti: Predmet[]) => {
        this.predmeti = predmeti;
      }
    );
  this.predmeti = this.predmetService.getPredmeti();
  }

  onNoviPredmet() {
    this.router.navigate(['new'], {relativeTo: this.route});
  }

}
