import { Component, OnInit, Input } from '@angular/core';
import { Predmet } from '../predmet.model';
import { Nastavnik } from 'src/app/nastavnici/nastavnik.model';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { NastavnikService } from 'src/app/nastavnici/nastavnik.service';
import { PredmetService } from '../predmet.service';
import { Predaje } from 'src/app/models/predaje.model';

@Component({
  selector: 'app-predmeti-predaje',
  templateUrl: './predmeti-predaje.component.html',
  styleUrls: ['./predmeti-predaje.component.css']
})
export class PredmetiPredajeComponent implements OnInit {

  
  
  ulogaLista = ['', 'NASTAVNIK', 'ASISTENT', 'DEMONSTRATOR'];
  uloga: string = null;

  predmet: Predmet;
  id: number;
  pohadja: Nastavnik[]=[];
  
  lista: Nastavnik[]=[];

  public selectedId:number;
  
  public highlightRow(emp: Nastavnik) {
    this.selectedId = emp.id;
    console.log(this.selectedId)
  }


  constructor(private nastavnikService: NastavnikService, private predmetService: PredmetService,
    private route: ActivatedRoute, private router: Router) {
     
     }

  ngOnInit() {

    this.route.params
    .subscribe(
      (params: Params) => {
        this.id = +params['id'];
        const predmetV: Predmet = this.predmetService.getPredmet(this.id);
        if(predmetV != null){
          this.predmet = predmetV
          this.lista = []
          this.pohadja = []
        }
        else{
          this.router.navigate(['/predmeti'])
        }
      }
    );
      this.lista= this.nastavnikService.getNastavnici();

      
      for(let item of this.lista){
        for(let nastavnik of this.predmet.predaje){
          if(item.id == nastavnik.nastavnik.id){
          //   var index = this.lista.map(x => {
          //     return x.id;
          //   }).indexOf(item.id);

          // this.lista.splice(index,1)
          this.lista = this.lista.filter(item => item.id != nastavnik.nastavnik.id);   
          }
        }
        
      }
      
     
  }
  onDodajNastavnika(id: number){
    //id = this.selectedId;
    console.log(id,this.predmet.id,this.uloga)
    this.predmetService.dodajProfesoraNaPredmetBaza(id, this.predmet.id,this.uloga).subscribe(
      (predaje: Predaje) => {
        this.predmetService.dodajProfesorNaPredmet(id,this.predmet.id,predaje);
        this.lista = this.lista.filter(item => item.id != predaje.nastavnik.id);
      }
    );
  }

  onOdustani() {
    this.router.navigate(['../'], {relativeTo: this.route});
  }
   
}
