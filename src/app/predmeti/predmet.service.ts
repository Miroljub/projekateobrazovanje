import { Injectable, EventEmitter } from '@angular/core';
import { Predmet } from './predmet.model';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Subject } from 'rxjs';
import { Pohadja } from '../models/pohadja.model';
import { UceniciService } from '../ucenici/ucenici.service';
import { Ucenik } from '../ucenici/ucenici.model';
import { Predaje } from '../models/predaje.model';
import { Nastavnik } from '../nastavnici/nastavnik.model';
import { NastavnikService } from '../nastavnici/nastavnik.service';
import { Ispit } from '../models/ispit';
import { Prijava } from '../models/prijava';
import { Ocenjivanje } from '../models/ocenjivanje';

@Injectable()
export class PredmetService{

    constructor(private httpClient: HttpClient,private ucenikServis:UceniciService, private nastavnikService: NastavnikService){}

  
    predmetChanged = new EventEmitter<Predmet[]>();
    private predmeti:Predmet[]=[];
    id:number;
    httpOptions = {
        headers: new HttpHeaders({
          'Content-Type':  'application/json',
          'Authorization': localStorage.getItem('token')
        })
      };
    
    

    getPredmeti(){
        return this.predmeti.slice();
    }

    getPredmetiBaza() {
        const httpOptions = {
            headers: new HttpHeaders({
              'Content-Type':  'application/json',
              'Authorization': localStorage.getItem('token')
            })
          };
        return this.httpClient.get<Predmet[]>('http://localhost:8080/api/predmeti',httpOptions);
    }

    getPredmet(index: number) {
        for(let predmet of this.predmeti) {
            if (predmet.id == index) {
                return predmet;
            }
        }
    }

    setPredmet(predmeti: Predmet[]){
        this.predmeti = predmeti;
        this.predmetChanged.next(this.predmeti.slice());
    }

    deletePredmet(id: number) {
        var index = this.predmeti.map(x => {
            return x.id;
          }).indexOf(id);

        this.predmeti.splice(index,1)

        this.predmetChanged.next(this.predmeti.slice());
    }  
    deletePredmetIzBaze(id:number){
        return this.httpClient.delete(`http://localhost:8080/api/predmeti/${id}`, this.httpOptions);
    }
    delteUcenikaSaPredmetaBaza(idU: number, idP: number){
        return this.httpClient.delete(`http://localhost:8080/api/predmeti/${idP}/pohadja/${idU}`, this.httpOptions)
    } 
    deleteUcenikaSaPredmeta(id: number,idP:number) {
        for(let pohadja of this.predmeti){
           
            var index = pohadja.pohadja.map(x => {
                return x.ucenik.id;
              }).indexOf(id);
        }   
      for(let predmet of this.predmeti){
            if(predmet.id == idP){
                predmet.pohadja.splice(index,1)
            }
      }
     
        this.predmetChanged.next(this.predmeti.slice());
    }

    dodajUcenikaNaPredmetBaza(idUcenik: number,idPredmet:number){
        return this.httpClient.post<Pohadja>(`http://localhost:8080/api/predmeti/${idPredmet}/pohadja/${idUcenik}`, {}, this.httpOptions);
    }

    dodajUCenikNaPredmet(idUcenik: number,idPredmet:number,pohadja:Pohadja){
        const ucenik: Ucenik = this.ucenikServis.getUcenik(idUcenik)
        for(let predmet of this.predmeti){
            if(predmet.id == idPredmet){
                predmet.pohadja.push(pohadja)
            }
        }

        this.predmetChanged.next(this.predmeti.slice());
    }

    dodajProfesoraNaPredmetBaza(idNastavnik: number,idPredmet:number,uloga:string){
        return this.httpClient.post<Predaje>(`http://localhost:8080/api/predmeti/${idPredmet}/predaje/${idNastavnik}`, {"uloga":uloga}, this.httpOptions);
    }
    dodajProfesorNaPredmet(idNastavnik: number,idPredmet:number,predaje: Predaje){
        // const nastavnik: Nastavnik = this.ucenikServis.getUcenik(idUcenik)
        const nastavnik: Nastavnik = this.nastavnikService.getNastavnik(idNastavnik)
        for(let predmet of this.predmeti){
            if(predmet.id == idPredmet){
                predmet.predaje.push(predaje)
            }
        }

        this.predmetChanged.next(this.predmeti.slice());
    }
    
    deleteProfesorSaPredmetaBaza(idPro: number, idP: number){
        return this.httpClient.delete(`http://localhost:8080/api/predmeti/${idP}/predaje/${idPro}`, this.httpOptions)
    }

    deleteProfesorSaPredmet(id: number,idP:number) {
        for(let pohadja of this.predmeti){
           
            var index = pohadja.predaje.map(x => {
                return x.nastavnik.id;
              }).indexOf(id);
        }   
      for(let predmet of this.predmeti){
            if(predmet.id == idP){
                predmet.predaje.splice(index,1)
            }
      }
     
        this.predmetChanged.next(this.predmeti.slice());
    }


    updatePredmet(predmetE: Predmet) {
       
        for(let predmet of this.predmeti){
            if(predmet.id == predmetE.id){
                predmet.espb = predmetE.espb;
                predmet.naziv = predmetE.naziv;
            }
       }      
        this.predmetChanged.next(this.predmeti.slice());
    }

    updateBazuPredmet(predmet:Predmet){
        return this.httpClient.put<Predmet>('http://localhost:8080/api/predmeti',predmet, this.httpOptions)
    }
    addPredmet(newPredmet: Predmet){
        const predmet:Predmet = new Predmet(newPredmet.id,newPredmet.naziv,newPredmet.espb,[],[])
        this.predmeti.push(predmet)
        this.predmetChanged.emit(this.predmeti.slice());
    }

    addUBazuPredmet(newPredmet: Predmet){
        return this.httpClient.post<Predmet>('http://localhost:8080/api/predmeti',newPredmet, this.httpOptions);
    }

    getPostojeciIspiti(id:number){
        return this.httpClient.get<Ispit[]>(`http://localhost:8080/api/predmeti/${id}/ispiti`,this.httpOptions)
    }

    getNePrijavljenjeIspite(id:number,idUcenika:number){
        return this.httpClient.get<Ispit[]>(`http://localhost:8080/api/predmeti/${id}/ispiti/neprijavljeni/${idUcenika}`,this.httpOptions)
    }

    dodajRok(ispit:Ispit){
        return this.httpClient.post<any>('http://localhost:8080/api/ispiti',ispit,this.httpOptions);
    }

    izbrisiRok(id:number){
        return this.httpClient.delete(`http://localhost:8080/api/ispiti/${id}`,this.httpOptions);
    }

    getPrijaveZaRok(id: number){
        return this.httpClient.get<Ispit>(`http://localhost:8080/api/ispiti/${id}`,this.httpOptions);
    }

    oceniUcenika(idIspita:number,idUcenika:number,brojBodova:number){
        return this.httpClient.put(`http://localhost:8080/api/ispiti/${idIspita}/prijave/${idUcenika}`,{"osvojeniBodovi":brojBodova},this.httpOptions);
    }

    getUcenikeKojiNisuPoloziliPredmet(id:number){
        return this.httpClient.get<Ucenik[]>(`http://localhost:8080/api/predmeti/${id}/nepolozeni`,this.httpOptions);
    }

    getIspite2(idPredmeta: number,idUcenika:number){
        return this.httpClient.get<Ocenjivanje[]>(`http://localhost:8080/api/predmeti/${idPredmeta}/oceni/${idUcenika}`,this.httpOptions)
    }

    oceni(idPredmet:number,idUcenik:number,konacnaOcena:number){
        return this.httpClient.put(`http://localhost:8080/api/predmeti/${idPredmet}/oceni/${idUcenik}`,{"ocena":konacnaOcena},this.httpOptions);
    }

}