import { Component, OnInit, OnDestroy } from '@angular/core';
import { PredmetService } from '../predmet.service';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { Predmet } from '../predmet.model';
import { Pohadja } from 'src/app/models/pohadja.model';
import { UceniciService } from 'src/app/ucenici/ucenici.service';
import { Predaje } from 'src/app/models/predaje.model';
import { NastavnikService } from 'src/app/nastavnici/nastavnik.service';
import { Ucenik } from 'src/app/ucenici/ucenici.model';
import { Nastavnik } from 'src/app/nastavnici/nastavnik.model';
import { componentRefresh } from '@angular/core/src/render3/instructions';

@Component({
  selector: 'app-predmeti-detalji',
  templateUrl: './predmeti-detalji.component.html',
  styleUrls: ['./predmeti-detalji.component.css']
})
export class PredmetiDetaljiComponent implements OnInit, OnDestroy {

  public searchTextPohadja : string;
  public searchTextPredaje : string;
  predmet: Predmet;
  id: number;
  pohadja: Ucenik[] = []
  sortirano: Ucenik[] = []
  
//  sort(ime){
//   //  console.log(this.pohadja)
//   const sortirano : Ucenik[] =this.pohadja.sort((a,b) => a.brojIndeksa.localeCompare(b.brojIndeksa));
//   this.pohadja = [];
//   this.pohadja = sortirano;
//   console.log(this.pohadja)

//   this.ngOnInit()
//  }
    
  constructor(private ucenikServis: UceniciService,private predmetService: PredmetService,
      private route: ActivatedRoute, private router: Router,
      private nastavnikServis: NastavnikService) { }

  ngOnInit() {
    
    
    
    this.route.params
    .subscribe(
      (params: Params) => {
        this.id = +params['id'];
        const predmetV: Predmet = this.predmetService.getPredmet(this.id);
        if(predmetV != null){
          this.predmet = predmetV
          this.pohadja =[];
          for(let item of this.predmet.pohadja){
        
            const ucenik:Ucenik = item.ucenik;
            
            this.pohadja.push(ucenik)
           
        }
          
        }
        else{
          this.router.navigate(['/predmeti'])
        }
      }

    );   
  }

  EditU(id: Number){
    this.router.navigate([`../ucenici/${id}`])
  }

  DeleteU(idU:number){
    this.predmetService.delteUcenikaSaPredmetaBaza(idU,this.id).subscribe(
      data => {
        this.predmetService.deleteUcenikaSaPredmeta(idU,this.id);
      }
    )
    this.pohadja = this.pohadja.filter(item => item.id != idU);   
  }

  EditP(id: Number){
    this.router.navigate([`../nastavnici/${id}`])
  }

  DeleteP(idUPro: number){
    this.predmetService.deleteProfesorSaPredmetaBaza(idUPro,this.id).subscribe(
      data => {
        this.predmetService.deleteProfesorSaPredmet(idUPro,this.id);
      }
    )
    this.predmet.predaje = this.predmet.predaje.filter(item => item.id != idUPro);   
  }


  onIzbrisiPredmet(){
    this.predmetService.deletePredmetIzBaze(this.id).subscribe(
      data => {
        this.predmetService.deletePredmet(this.id); 
      }
    );
      
    this.router.navigate(['/predmeti']);
  }

  onIzmeniPredmet() {
    this.router.navigate(['edit'], {relativeTo: this.route});
  }

  onDodajUcenikaNaPredmet(){
    this.router.navigate(['pohadja'], {relativeTo: this.route});
  }

  onDodajNastavnikaNaPredmet(){
    this.router.navigate(['predaje'], {relativeTo: this.route});
  }

  ngOnDestroy(){
    this.pohadja =[]
  }

}
