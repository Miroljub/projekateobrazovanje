import { Component, OnInit, OnDestroy } from '@angular/core';
import { Predmet } from '../predmet.model';
import { Ucenik } from 'src/app/ucenici/ucenici.model';
import { UceniciService } from 'src/app/ucenici/ucenici.service';
import { PredmetService } from '../predmet.service';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { Pohadja } from 'src/app/models/pohadja.model';

@Component({
  selector: 'app-predmeti-pohadja',
  templateUrl: './predmeti-pohadja.component.html',
  styleUrls: ['./predmeti-pohadja.component.css']
})
export class PredmetiPohadjaComponent implements OnInit , OnDestroy {

  searchText:string;
  predmet: Predmet;
  id: number;
  pohadja: Ucenik[]=[];
  
  lista: Ucenik[]=[];

  constructor(private ucenikService: UceniciService, private predmetService: PredmetService,
    private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {

    this.route.params
    .subscribe(
      (params: Params) => {
        this.id = +params['id'];
        const predmetV: Predmet = this.predmetService.getPredmet(this.id);
        if(predmetV != null){
          this.predmet = predmetV
          this.lista = []
          this.pohadja = []
        }
        else{
          this.router.navigate(['/predmeti'])
        }
      }
    );
      this.lista= this.ucenikService.getUcenici();

      
      for(let item of this.lista){
        for(let ucenik of this.predmet.pohadja){
          if(item.id == ucenik.ucenik.id){
          //   var index = this.lista.map(x => {
          //     return x.id;
          //   }).indexOf(item.id);

          // this.lista.splice(index,1)
          this.lista = this.lista.filter(item => item.id != ucenik.ucenik.id);   
          }
        }
        
      }
      

    
  }
 //dodaj ucenika
  DodajUcenika(id: number){
    this.predmetService.dodajUcenikaNaPredmetBaza(id,this.predmet.id).subscribe(
      (pohadja: Pohadja) => {
        this.predmetService.dodajUCenikNaPredmet(id,this.predmet.id,pohadja);
        this.lista = this.lista.filter(item => item.id != pohadja.ucenik.id); 
      }
    )
  }

  onOdustani() {
    this.router.navigate(['../'], {relativeTo: this.route});
  }
  ngOnDestroy(){
    this.lista = [];
    this.pohadja = [],
    this.predmet = null
  }

}