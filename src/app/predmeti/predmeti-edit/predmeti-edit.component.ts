import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { PredmetService } from '../predmet.service';
import { Predmet } from '../predmet.model';

@Component({
  selector: 'app-predmeti-edit',
  templateUrl: './predmeti-edit.component.html',
  styleUrls: ['./predmeti-edit.component.css']
})
export class PredmetiEditComponent implements OnInit {

  id: number;
  editMode = false;
  predmetForm: FormGroup;

  constructor(private route: ActivatedRoute, private predmetService:PredmetService, private router: Router) { }

  ngOnInit() {
    this.route.params
    .subscribe(
      (params: Params) => {
        this.id = +params['id'];
        this.editMode = params['id'] != null;
        this.initForm();
      }
    );
  }

  onSubmit() {
    
    if (this.editMode) {
      const predmetE:Predmet = new Predmet(this.id,this.predmetForm.value.naziv,this.predmetForm.value.espb,[],[])
      this.predmetService.updateBazuPredmet(predmetE).subscribe(
        (predmet:Predmet) =>{
          this.predmetService.updatePredmet(predmet);
        }
      )
      
    } else {
      this.predmetService.addUBazuPredmet(this.predmetForm.value).subscribe(
        (predmet: Predmet) => {
          this.predmetService.addPredmet(predmet)
          
        }
      );
    }
    this.onOdustani();
    //console.log(this.predmetForm.value);
  }

  onOdustani() {
    this.router.navigate(['../'], {relativeTo: this.route});
  }

  private initForm(){
    let predmetNaziv = '';
    let predmetBodovi = 0 ;

    if (this.editMode) {
      const predmet = this.predmetService.getPredmet(this.id);
      predmetNaziv = predmet.naziv;
      predmetBodovi = predmet.espb;
    }

    this.predmetForm = new FormGroup({
      'naziv': new FormControl(predmetNaziv, Validators.required),
      'espb': new FormControl(predmetBodovi, Validators.required),
    });
  }


}
