import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Predmet } from './predmet.model';
import { PredmetService } from './predmet.service';

@Component({
  selector: 'app-predmeti',
  templateUrl: './predmeti.component.html',
  styleUrls: ['./predmeti.component.css']
})
export class PredmetiComponent implements OnInit {

  predmeti: Predmet[];

  constructor(private httpClient:HttpClient,
              private predmetService: PredmetService) { }

  ngOnInit() {
    this.predmetService.getPredmetiBaza().subscribe(
      (predmeti: Predmet[]) => {
        this.predmetService.setPredmet(predmeti)
      });
    this.predmetService.predmetChanged.subscribe(
      (predmeti: Predmet[]) => {
        this.predmeti = predmeti
      }
    )
      
  };

}
