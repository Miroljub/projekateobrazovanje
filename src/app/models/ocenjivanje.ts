export class Ocenjivanje{
    constructor(
        public tipIspita:string,
        public nastavispitniRoknik:string,
        public maksimalniBodovi:string,        
        public osvojeniBodovi:string
    ){}
}