import { Nastavnik } from '../nastavnici/nastavnik.model';
import { Predmet } from '../predmeti/predmet.model';

export class NastavnikPredaje{
    constructor(
        public id:number,
        public nastavnik:Nastavnik,
        public predmet:Predmet,
        
        public uloga:String
    ){}
}