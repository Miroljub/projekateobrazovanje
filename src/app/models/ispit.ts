import { Prijava } from './prijava';

export class Ispit {
    constructor(
      public id: number,
      public ispitniRok:string,
      public predmet: number,
      public maksimumBodova: number,
      public tipIspita: string,
      public prijave: Prijava[]
   ){}
}