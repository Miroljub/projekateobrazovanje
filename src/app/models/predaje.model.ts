import { Predmet } from '../predmeti/predmet.model';
import { Nastavnik } from '../nastavnici/nastavnik.model';

export class Predaje{
    constructor(
        public id:number,
        public predmet:Predmet,
        public nastavnik:Nastavnik,
        public uloga:String
    ){}
}