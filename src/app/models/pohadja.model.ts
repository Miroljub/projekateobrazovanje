import { Ucenik } from '../ucenici/ucenici.model';

export class Pohadja{
    constructor(
        public id:number,
        public konacnaOcena:number,
        public ucenik:Ucenik
    ){}
}