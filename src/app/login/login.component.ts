import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormControl, Validators, EmailValidator } from '@angular/forms';
import { Router, UrlTree } from '@angular/router';
import { AuthService } from '../servies/authService.service';
import { HttpResponse, HttpResponseBase } from '@angular/common/http';
import { Response } from 'selenium-webdriver/http';
import { Config } from 'protractor';
import { UceniciService } from '../ucenici/ucenici.service';
import { Ucenik } from '../ucenici/ucenici.model';
import { Predmet } from '../predmeti/predmet.model';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  
  valid: boolean =true;
  token: string='';
  header:Headers;
  signinForm: FormGroup;
  headers: string[];
  @Input('email') ime: string;
  constructor(private router:Router,private authService:AuthService,private ucenikS:UceniciService) { }

  ngOnInit() {
    this.signinForm = new FormGroup({
      'email': new FormControl(null,Validators.required),
      'password' : new FormControl(null, Validators.required)  
      }); 
    } 

    login() {
      const val = this.signinForm.value;

      if (val.email && val.password) {
          this.authService.login(val.email, val.password).subscribe(
            resp=>{  
                    localStorage.setItem('token',resp.headers.get('Authorization')),
                    this.authService.preuzmiUsera(val.email),
                    this.valid = true,
                    this.signinForm.reset()},
            error => {console.log(error),
                      this.signinForm.reset(),
                      this.valid = false},
            )   
      }
    }

}