import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Response } from 'selenium-webdriver/http';
import { Predmet } from '../predmeti/predmet.model';
import { Subject } from 'rxjs';


const httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
      'Authorization': ''
    })
  };

@Injectable()
export class AuthService {
    token: boolean = false;
    tokenChange = new Subject<boolean>();

    ulogovanUser: any;

    
    constructor(private http: HttpClient) {
    }   
    
    login(username:string,password:string) {
        return this.http.post<Response>('http://localhost:8080/login',{username,password},{ observe: 'response' })
    }

    preuzmiUsera(userName){
      return this.http.get<any>(`http://localhost:8080/users/${userName}`).subscribe(
      data => {localStorage.setItem('role',data.role);
              localStorage.setItem('id', data.userId);
                this.token = true,
                this.tokenChange.next(this.token)
                }
      )

    }

    logOut(){
      this.token = false;
      this.tokenChange.next(this.token);
    }

    getUceniciBaza2() {

        const httpOptions = {
            headers: new HttpHeaders({
              'Content-Type':  'application/json',
              'Authorization': localStorage.getItem('token')
            })
          };
        return this.http.get<Predmet[]>('http://localhost:8080/api/predmeti', httpOptions);
    }

    addUBazuPredmet(){
        const newPredmet: Predmet = new Predmet(1,"Eobrazovanje",12,[],[]);
        return this.http.post<Predmet>('http://localhost:8080/api/predmeti',newPredmet,httpOptions);
    }

    




}

  