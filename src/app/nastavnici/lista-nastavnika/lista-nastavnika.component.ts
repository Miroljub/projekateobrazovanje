import { Component, OnInit } from '@angular/core';
import { Nastavnik } from '../nastavnik.model';
import { NastavnikService } from '../nastavnik.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-lista-nastavnika',
  templateUrl: './lista-nastavnika.component.html',
  styleUrls: ['./lista-nastavnika.component.css']
})
export class ListaNastavnikaComponent implements OnInit {

  nastavnici: Nastavnik[] = [];
  public searchText : string;

  constructor(private nastavnikServis: NastavnikService, private router: Router, private route: ActivatedRoute) { }

  ngOnInit() {
    this.nastavnikServis.nastavnikChanged
      .subscribe(
        (nastavnici: Nastavnik[]) => {
          this.nastavnici = nastavnici;
        }
      );
    this.nastavnici = this.nastavnikServis.getNastavnici();

  }

  onNoviNastavnik() {
    this.router.navigate(['new'], {relativeTo: this.route});
  }

}
