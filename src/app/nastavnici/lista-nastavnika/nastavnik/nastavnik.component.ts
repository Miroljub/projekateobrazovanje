import { Component, OnInit, Input } from '@angular/core';
import { Nastavnik } from '../../nastavnik.model';

@Component({
  selector: 'app-nastavnik',
  templateUrl: './nastavnik.component.html',
  styleUrls: ['./nastavnik.component.css']
})
export class NastavnikComponent implements OnInit {

  @Input() nastavnik: Nastavnik;
  @Input() index: number;

  constructor() { }

  ngOnInit() {
  }

  
}
