import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { NastavnikService } from '../nastavnik.service';
import { Nastavnik } from '../nastavnik.model';

@Component({
  selector: 'app-nastavnici-edit',
  templateUrl: './nastavnici-edit.component.html',
  styleUrls: ['./nastavnici-edit.component.css']
})
export class NastavniciEditComponent implements OnInit {
  id: number;
  editMode = false;
  nastavnikForm: FormGroup;

  constructor(private route: ActivatedRoute, private nastavnikServis: NastavnikService, private router: Router) { }

  ngOnInit() {
    this.route.params
    .subscribe(
      (params: Params) => {
        this.id = +params['id'];
        this.editMode = params['id'] != null;
        if(this.editMode == true && this.nastavnikServis.getNastavnik(this.id) == null){
          this.editMode = false;
          this.router.navigate(['../'], {relativeTo: this.route});
          this.initForm()
        }
        else{
          this.initForm()
        }

        
        
      }
    );
  }


  onSubmit() {
    
    if (this.editMode) {
      const nastavnikE:Nastavnik = new Nastavnik(this.id,this.nastavnikForm.value.ime,this.nastavnikForm.value.prezime,
                                           this.nastavnikForm.value.username,"",
                                            this.nastavnikForm.value.datumRodjenja,this.nastavnikForm.value.mestoRodjenja,[])
      this.nastavnikServis.updateBazuNastavnik(nastavnikE).subscribe(
        (nastavnik:Nastavnik) =>{
          this.nastavnikServis.updateNastavnik(nastavnik);
          this.onOdustani();
        },
        error => {
          if(error.status == 500){            
            this.nastavnikForm.controls['username'].setErrors({'incorrect': true});
            console.log(this.nastavnikForm.status);           
          }}
      )

    } else {
      
      this.nastavnikServis.addUBazuNastavnika(this.nastavnikForm.value).subscribe(
        (nastavnik: Nastavnik) => {         
          this.nastavnikServis.addNastavnik(nastavnik);
          this.onOdustani();
        },
        error => {
          if(error.status == 400){            
            this.nastavnikForm.controls['username'].setErrors({'incorrect': true});
            console.log(this.nastavnikForm.status);           
          }}
      );
      
    } 
    
  }

  onOdustani() {
    this.router.navigate(['../'], {relativeTo: this.route});
  }
  onDalje(){
    return;
  }

  private initForm() {
    let nastavnikIme = '';
    let nastavnikPrezime = '';
    let nastavnikUsername = '';
    let nastavnikPassword = '';
    let nastavnikDatumRodjenja = '';
    let nastavnikMestoRodjenja = '';
    

    if (this.editMode) {
      const nastavnik = this.nastavnikServis.getNastavnik(this.id);
      nastavnikIme = nastavnik.ime;
      nastavnikPrezime = nastavnik.prezime;
      nastavnikDatumRodjenja = nastavnik.datumRodjenja.substring(0,10);
      nastavnikMestoRodjenja = nastavnik.mestoRodjenja;
      nastavnikUsername = nastavnik.username;
      nastavnikPassword = nastavnik.password;
      
    }
    if(this.editMode)
    {
      //Zbog promene validatora za password
      this.nastavnikForm = new FormGroup({
        'ime': new FormControl(nastavnikIme, Validators.required),
        'prezime': new FormControl(nastavnikPrezime, Validators.required),
        'username': new FormControl(nastavnikUsername, Validators.required),
        'datumRodjenja': new FormControl(nastavnikDatumRodjenja, Validators.required),
        'mestoRodjenja': new FormControl(nastavnikMestoRodjenja, Validators.required),
      });
    }else{
      this.nastavnikForm = new FormGroup({
        'ime': new FormControl(nastavnikIme, Validators.required),
        'prezime': new FormControl(nastavnikPrezime, Validators.required),
        'username': new FormControl(nastavnikUsername, Validators.required),
        'password': new FormControl(nastavnikPassword, Validators.required),
        'datumRodjenja': new FormControl(nastavnikDatumRodjenja, Validators.required),
        'mestoRodjenja': new FormControl(nastavnikMestoRodjenja, Validators.required),
      });
    }

   
  }

}
