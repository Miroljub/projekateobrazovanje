import { Component, OnInit } from '@angular/core';
import { Nastavnik } from '../nastavnik.model';
import { NastavnikService } from '../nastavnik.service';
import { ActivatedRoute, Router, Params } from '@angular/router';

@Component({
  selector: 'app-nastavnici-detalji',
  templateUrl: './nastavnici-detalji.component.html',
  styleUrls: ['./nastavnici-detalji.component.css']
})
export class NastavniciDetaljiComponent implements OnInit {
  nastavnik: Nastavnik= <Nastavnik>{};
  id: number;
  admin:boolean = true;
  role:string ="";

  constructor(private nastavnikServis: NastavnikService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.route.params
      .subscribe(
        (params: Params) => {
          this.id = +params['id'];         
          this.nastavnikServis.getNastavnikIzBaze(this.id).subscribe(
            (nastavnik: Nastavnik) => this.nastavnik = nastavnik
          )
          // const nastavnikV: Nastavnik = this.nastavnikServis.getNastavnik(this.id);
          // if(nastavnikV != null){
          //   this.nastavnik = nastavnikV
          // }
          // else{
          //   this.router.navigate(['/nastavnici'])
          // }
        }
      );
      
      this.role = localStorage.getItem('role');

      if(this.role == "ADMIN"){
        this.admin = true;
      }else{
        this.admin = false;
      }

      this.nastavnikServis.nastavnikPromenjen.subscribe(
        (nastavnik : Nastavnik) => this.nastavnik = nastavnik
      )

      
  }

  onIzmeniNastavnika() {
    this.router.navigate(['edit'], {relativeTo: this.route});
  }

  onIzbrisiNastavnika(){
    this.nastavnikServis.deleteNastavnikIzBaze(this.id).subscribe(
      data => {
        this.nastavnikServis.deleteNastavnik(this.id); 
      }
    );
      
    this.router.navigate(['/nastavnici']);
  }

  onIzmeniPassword(){
    this.router.navigate(['promenaPassword'], {relativeTo: this.route});

  }


}
