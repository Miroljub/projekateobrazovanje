import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Nastavnik } from './nastavnik.model';
import { NastavnikService } from './nastavnik.service';

@Component({
  selector: 'app-nastavnici',
  templateUrl: './nastavnici.component.html',
  styleUrls: ['./nastavnici.component.css']
})
export class NastavniciComponent implements OnInit {

  
  nastavnici: Nastavnik[];

  constructor(private httpClient:HttpClient,
              private nastavniciService:NastavnikService) { }

  ngOnInit() {    
    this.nastavniciService.getNastavniciBaza().subscribe(
      (nastavnici: Nastavnik[]) => {
        this.nastavniciService.setNastavnik(nastavnici)
      });
  };

  }

  

 
