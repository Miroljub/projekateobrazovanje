import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Nastavnik } from '../nastavnik.model';
import { NastavnikService } from '../nastavnik.service';
import { empty } from 'rxjs';

@Component({
  selector: 'app-nastavnici-password',
  templateUrl: './nastavnici-password.component.html',
  styleUrls: ['./nastavnici-password.component.css']
})
export class NastavniciPasswordComponent implements OnInit {
  id: number;
  nastavnik: Nastavnik = new Nastavnik;
  @Input('oldPassowrd') oldPassowrd = "";
  @Input('newPassword') newPassword = "";
  @Input('repeatPassword') repeatPassword = "";
  @Input() vidi = "password"
  provera = true;
  stari = true;

  constructor(private route: ActivatedRoute,private nastavnikServis: NastavnikService,private router: Router) { }

  ngOnInit() {
    this.route.params.subscribe(
      (params:Params) => {
        this.id = params['id'];

        if(this.id != null){
          this.nastavnikServis.getNastavnikBaza(this.id).subscribe(
            (nastavnik:Nastavnik) => this.nastavnik = nastavnik
            
          )
        }}
    )

  }

  promeni(){
    if(this.newPassword != this.repeatPassword){
      this.provera = false;
      this.stari = true;
    }else{
      this.provera = true;
      this.stari = true;
      this.nastavnikServis.promenaPassworda(this.oldPassowrd,this.newPassword,this.nastavnik.id).subscribe(
        data =>  this.router.navigate(['../'], {relativeTo: this.route}),
        error => {
          if (error.status == 403){
              this.stari = false;
          }
        }
      )
    }


  }

  pogledaj(){
    if(this.vidi == "password"){
      this.vidi = "text"
    }else{
      this.vidi = "password"
    }
  }


  odustani(){
    this.router.navigate(['../'], {relativeTo: this.route})
  }
}
