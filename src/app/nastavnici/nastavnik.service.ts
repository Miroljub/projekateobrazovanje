import { Nastavnik } from './nastavnik.model';
import { Subject } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Predaje } from '../models/predaje.model';

@Injectable()
export class NastavnikService{

    nastavnikChanged = new Subject<Nastavnik[]>();
    nastavnikSelected = new Subject<Nastavnik>();
    nastavnikPromenjen = new Subject<Nastavnik>();
    id:number;
    httpOptions = {
        headers: new HttpHeaders({
          'Content-Type':  'application/json',
          'Authorization': localStorage.getItem('token')
        })
      };

    

    constructor(private httpClient: HttpClient){}   

    private nastavnici: Nastavnik[]= [
        // new Nastavnik(0,'Dusan', 'Vukovic', '27.02.1994', 'Novi Sad',[]),
        // new Nastavnik(1,'Miroljub', 'Marjanovic', '27.05.1995', 'Beograd',[]),
    ]    

    setNastavnik(nastavnici: Nastavnik[]){
        this.nastavnici = nastavnici,
        this.nastavnikChanged.next(this.nastavnici.slice());
    }

    getNastavnici(){
        return this.nastavnici.slice();
    }

    getNastavnik(index: number){
        for(let nastavnik of this.nastavnici){
            if(nastavnik.id == index){
                return nastavnik;
            }
            
        }
        return null;
    }

    updateNastavnik(nastavnikE: Nastavnik) {
        
        for(let nastavnik of this.nastavnici){
            if(nastavnik.id == nastavnikE.id){
                nastavnik.ime = nastavnikE.ime;
                nastavnik.prezime = nastavnikE.prezime;
                nastavnik.username = nastavnikE.username;
                nastavnik.datumRodjenja = nastavnikE.datumRodjenja;
                nastavnik.mestoRodjenja = nastavnikE.mestoRodjenja;
            }
       }      
        this.nastavnikChanged.next(this.nastavnici.slice());
        this.nastavnikPromenjen.next(nastavnikE);
    }
    updateBazuNastavnik(nastavnik:Nastavnik){
           console.log(this.httpOptions)     
        return this.httpClient.put<Nastavnik>('http://localhost:8080/api/nastavnici',nastavnik ,this.httpOptions)
    }


    deleteNastavnik(id: number) {
        var index = this.nastavnici.map(x => {
            return x.id;
          }).indexOf(id);

        this.nastavnici.splice(index,1)

        this.nastavnikChanged.next(this.nastavnici.slice());
    }   
    deleteNastavnikIzBaze(id:number){
        return this.httpClient.delete(`http://localhost:8080/api/nastavnici/${id}`,this.httpOptions);
    } 

    getNastavnikIzBaze(id:number){
         const httpOptions = {
            headers: new HttpHeaders({
              'Content-Type':  'application/json',
              'Authorization': localStorage.getItem('token')
            })
          };
        return this.httpClient.get<Nastavnik>(`http://localhost:8080/api/nastavnici/${id}`,this.httpOptions);
    } 


    addNastavnik(newNastavnik: Nastavnik){
        
        this.nastavnici.push(newNastavnik);
        this.id = null;
        this.nastavnikChanged.next(this.nastavnici.slice());
    }
    addUBazuNastavnika(newNastavnik: Nastavnik){        
        return this.httpClient.post<Nastavnik>('http://localhost:8080/api/nastavnici',newNastavnik,this.httpOptions);
    }

    getNastavniciBaza() {
        const httpOptions = {
            headers: new HttpHeaders({
              'Content-Type':  'application/json',
              'Authorization': localStorage.getItem('token')
            })
          };

        
        return this.httpClient.get<Nastavnik[]>('http://localhost:8080/api/nastavnici',this.httpOptions);
    }

    getNastavnikBaza(id:number) {
        const httpOptions = {
            headers: new HttpHeaders({
              'Content-Type':  'application/json',
              'Authorization': localStorage.getItem('token')
            })
          };

        
        return this.httpClient.get<Nastavnik>(`http://localhost:8080/api/nastavnici/${id}`,this.httpOptions);
    }

    getPredajeBaza(id: number){
        return this.httpClient.get<Predaje[]>(`http://localhost:8080/api/nastavnici/${id}/predaje`,this.httpOptions);
    }

    promenaPassworda(stariPassword:string, noviPassword: string, id:number){
        const httpOptions = {
            headers: new HttpHeaders({
              'Content-Type':  'application/json',
              'Authorization': localStorage.getItem('token')
            })
          };
        return this.httpClient.put(`http://localhost:8080/api/nastavnici/${id}/changePass`,
        {
            "newPassword": noviPassword,
            "oldPassword": stariPassword}
            ,httpOptions)
    }
}