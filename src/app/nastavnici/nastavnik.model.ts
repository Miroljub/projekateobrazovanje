export class Nastavnik{
    constructor(
                public id?:number,
                public ime?: string,
                public prezime?: string, 
                public username?: string,
                public password?: string,
                public datumRodjenja?: string, 
                public mestoRodjenja?:string,
                public predaje?: string[]
                ){}
}