import { Component, OnInit } from '@angular/core';
import { NastavnikService } from './nastavnici/nastavnik.service';
import { UceniciService } from './ucenici/ucenici.service';
import { PredmetService } from './predmeti/predmet.service';
import { Ucenik } from './ucenici/ucenici.model';
import { Predmet } from './predmeti/predmet.model';
import { Nastavnik } from './nastavnici/nastavnik.model';
import { AuthService } from './servies/authService.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  title = ' Miroljub i Dusan';
  token: boolean = false;
  
  constructor(private authServis: AuthService){}

  ngOnInit(){
    if(localStorage.getItem('token')){
      this.token = true;
    }
    this.authServis.tokenChange.subscribe(
      token => this.token = token
    )
    
    
  }
}
