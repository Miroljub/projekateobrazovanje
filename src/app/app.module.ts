import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import {NgxPaginationModule} from 'ngx-pagination'

import { AppComponent } from './app.component';
import { PredmetiComponent } from './predmeti/predmeti.component';
import { UceniciComponent } from './ucenici/ucenici.component';
import { NastavniciComponent } from './nastavnici/nastavnici.component';
import { UplateComponent } from './uplate/uplate.component';
import { HeaderComponent } from './header/header.component';
import { DropdownDirective } from './header/dropdown.directive';
import { UceniciListaComponent } from './ucenici/ucenici-lista/ucenici-lista.component';
import { UceniciItemComponent } from './ucenici/ucenici-lista/ucenici-item/ucenici-item.component';
import { UceniciService } from './ucenici/ucenici.service';
import { AppRoutingModule } from './app-routing-module';
import { ListaNastavnikaComponent } from './nastavnici/lista-nastavnika/lista-nastavnika.component';
import { NastavnikComponent } from './nastavnici/lista-nastavnika/nastavnik/nastavnik.component';
import { NastavnikService } from './nastavnici/nastavnik.service';
import {HttpClientModule} from '@angular/common/http';
import { UceniciStartComponent } from './ucenici/ucenici-start/ucenici-start.component';
import { UceniciDetaljiComponent } from './ucenici/ucenici-detalji/ucenici-detalji.component';
import { UceniciEditComponent } from './ucenici/ucenici-edit/ucenici-edit.component';
import { NastavniciDetaljiComponent } from './nastavnici/nastavnici-detalji/nastavnici-detalji.component';
import { NastavniciEditComponent } from './nastavnici/nastavnici-edit/nastavnici-edit.component';
import { NastavniciStartComponent } from './nastavnici/nastavnici-start/nastavnici-start.component';
import { PredmetiListaComponent } from './predmeti/predmeti-lista/predmeti-lista.component';
import { PredmetComponent } from './predmeti/predmeti-lista/predmet/predmet.component';
import { PredmetiDetaljiComponent } from './predmeti/predmeti-detalji/predmeti-detalji.component';
import { PredmetiEditComponent } from './predmeti/predmeti-edit/predmeti-edit.component'
import { PredmetService } from './predmeti/predmet.service';
import { PredmetiStartComponent } from './predmeti/predmeti-start/predmeti-start.component';
import { SearchFilter } from './models/search';
import { PredmetiPohadjaComponent } from './predmeti/predmeti-pohadja/predmeti-pohadja.component';
import { PredmetiPredajeComponent } from './predmeti/predmeti-predaje/predmeti-predaje.component';
import { LoginComponent } from './login/login.component';
import { AuthService } from './servies/authService.service';
import { HomeComponent } from './home/home.component';
import { UcenikLicniPodaciComponent } from './ucenici/ucenik-licni-podaci/ucenik-licni-podaci.component';
import { UcenikPodaciEditComponent } from './ucenici/ucenik-licni-podaci/ucenik-podaci-edit/ucenik-podaci-edit.component';
import { UcenikUplatnicaComponent } from './uplate/ucenik-uplatnica/ucenik-uplatnica.component';
import { IzmenaRacunaComponent } from './models/izmena-racuna/izmena-racuna.component';
import { UcenikComponent } from './ucenik/ucenik.component';
import { PremtetiKojePohadjaComponent } from './ucenik/premteti-koje-pohadja/premteti-koje-pohadja.component';
import { PredmetKojiPohadjaComponent } from './ucenik/premteti-koje-pohadja/predmet-koji-pohadja/predmet-koji-pohadja.component';
import { PredmetKojiPohadjaDetaljiComponent } from './ucenik/predmet-koji-pohadja-detalji/predmet-koji-pohadja-detalji.component';
import { NastavnikPanelComponent } from './nastavnik-panel/nastavnik-panel.component';
import { ListaPredmetaNastavnikComponent } from './nastavnik-panel/lista-predmeta-nastavnik/lista-predmeta-nastavnik.component';
import { PredmetNastavnikComponent } from './nastavnik-panel/lista-predmeta-nastavnik/predmet-nastavnik/predmet-nastavnik.component';
import { PredmetNastavnikDetaljiComponent } from './nastavnik-panel/predmet-nastavnik-detalji/predmet-nastavnik-detalji.component';
import { RokComponent } from './nastavnik-panel/rok/rok.component';
import { OceniRokComponent } from './nastavnik-panel/oceni-rok/oceni-rok.component';
import { PolozeniPredmetiComponent } from './ucenik/polozeni-predmeti/polozeni-predmeti.component';
import { NepolozeniPredmetiComponent } from './ucenik/nepolozeni-predmeti/nepolozeni-predmeti.component';
import { OceniUcenikeComponent } from './nastavnik-panel/oceni-ucenike/oceni-ucenike.component';
import { DavanjeOceneComponent } from './nastavnik-panel/davanje-ocene/davanje-ocene.component';
import { PrijavaIspitaComponent } from './ucenik/prijava-ispita/prijava-ispita.component';
import { ListaNepolozenihPredmetaComponent } from './ucenik/prijava-ispita/lista-nepolozenih-predmeta/lista-nepolozenih-predmeta.component';
import { NepolezeniPredmetComponent } from './ucenik/prijava-ispita/lista-nepolozenih-predmeta/nepolezeni-predmet/nepolezeni-predmet.component';
import { PrijaviIspitComponent } from './ucenik/prijava-ispita/prijavi-ispit/prijavi-ispit.component';
import { IstorijaUplataComponent } from './ucenik/istorija-uplata/istorija-uplata.component';
import { NastavniciPasswordComponent } from './nastavnici/nastavnici-password/nastavnici-password.component';

@NgModule({
  declarations: [
    AppComponent,
    PredmetiComponent,
    UceniciComponent,
    NastavniciComponent,
    UplateComponent,
    HeaderComponent,
    DropdownDirective,
    UceniciListaComponent,
    UceniciItemComponent,
    DropdownDirective,
    ListaNastavnikaComponent,
    NastavnikComponent,
    DropdownDirective,
    UceniciListaComponent,
    UceniciItemComponent,
    DropdownDirective,
    UceniciStartComponent,
    UceniciDetaljiComponent,
    UceniciEditComponent,
    NastavniciDetaljiComponent,
    NastavniciEditComponent,
    NastavniciStartComponent,
    PredmetiListaComponent,
    PredmetComponent,
    PredmetiDetaljiComponent,
    PredmetiEditComponent,
    PredmetiStartComponent,
    SearchFilter,
    PredmetiPohadjaComponent,
    PredmetiPredajeComponent,
    LoginComponent,
    HomeComponent,
    UcenikLicniPodaciComponent,
    UcenikPodaciEditComponent,
    UcenikUplatnicaComponent,
    IzmenaRacunaComponent,
    NastavnikPanelComponent,
    ListaPredmetaNastavnikComponent,
    PredmetNastavnikComponent,
    UcenikComponent,
    PremtetiKojePohadjaComponent,
    PredmetKojiPohadjaComponent,
    PredmetKojiPohadjaDetaljiComponent,
    PredmetNastavnikDetaljiComponent,
    RokComponent,
    PolozeniPredmetiComponent,
    NepolozeniPredmetiComponent,
    OceniRokComponent,
    PolozeniPredmetiComponent,
    OceniUcenikeComponent,
    DavanjeOceneComponent,
    PrijavaIspitaComponent,
    ListaNepolozenihPredmetaComponent,
    NepolezeniPredmetComponent,
    PrijaviIspitComponent,
    IstorijaUplataComponent,
    NastavniciPasswordComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    HttpClientModule,
    NgxPaginationModule

  ],
  providers: [UceniciService,NastavnikService,PredmetService,AuthService],
  bootstrap: [AppComponent]
})
export class AppModule { }
