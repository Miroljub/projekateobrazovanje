import { Component, OnInit } from '@angular/core';
import { Predmet } from 'src/app/predmeti/predmet.model';
import { UceniciService } from 'src/app/ucenici/ucenici.service';

@Component({
  selector: 'app-nepolozeni-predmeti',
  templateUrl: './nepolozeni-predmeti.component.html',
  styleUrls: ['./nepolozeni-predmeti.component.css']
})
export class NepolozeniPredmetiComponent implements OnInit {

  id: number;
  predmeti: Predmet[];

  constructor(private uceniciService: UceniciService) { }

  ngOnInit() {
    this.id = +localStorage.getItem('id');
    this.uceniciService.getNepolozeniPredmetiBaza(this.id).subscribe(
      (predmeti: Predmet[]) => {
        this.predmeti = predmeti;
        console.log(predmeti);
      }
    );
  }

}
