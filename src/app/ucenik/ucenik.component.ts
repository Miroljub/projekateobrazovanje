import { Component, OnInit } from '@angular/core';
import { Predmet } from '../predmeti/predmet.model';
import { UceniciService } from '../ucenici/ucenici.service';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Ucenik } from '../ucenici/ucenici.model';
import { PredmetService } from '../predmeti/predmet.service';

@Component({
  selector: 'app-ucenik',
  templateUrl: './ucenik.component.html',
  styleUrls: ['./ucenik.component.css']
})
export class UcenikComponent implements OnInit {

  ucenik: Ucenik;
  id: number;
  predmeti: Predmet[];

  constructor(private predmetService: PredmetService, private uceniciService: UceniciService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
  this.id = +localStorage.getItem('id');
  this.uceniciService.getPredmetiKojePohadjaUcenikBaza(this.id).subscribe(
    (predmeti: Predmet[]) => {
      console.log(predmeti);
      this.predmeti = predmeti;
      console.log(this.predmeti);
    });
  }

}
