import { Component, OnInit } from '@angular/core';
import { UceniciService } from 'src/app/ucenici/ucenici.service';
import { Predmet } from 'src/app/predmeti/predmet.model';
import { Pohadja } from 'src/app/models/pohadja.model';

@Component({
  selector: 'app-polozeni-predmeti',
  templateUrl: './polozeni-predmeti.component.html',
  styleUrls: ['./polozeni-predmeti.component.css']
})
export class PolozeniPredmetiComponent implements OnInit {

  id: number;
  predmeti: Predmet[];
  konacnaOcenaLista: number[]=[];
  konOcena: number;
  pohadjaLista: Pohadja[]=[];

  constructor(private uceniciService: UceniciService) { }

  ngOnInit() {
    this.id = +localStorage.getItem('id');
    this.uceniciService.getPolozeniPredmetiBaza(this.id).subscribe(
      (predmeti: Predmet[]) => {
        this.predmeti = predmeti;
        console.log(predmeti);
      }
    );
  }

}
