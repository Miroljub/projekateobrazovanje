import { Component, OnInit, Input } from '@angular/core';
import { Predmet } from 'src/app/predmeti/predmet.model';

@Component({
  selector: 'app-premteti-koje-pohadja',
  templateUrl: './premteti-koje-pohadja.component.html',
  styleUrls: ['./premteti-koje-pohadja.component.css']
})
export class PremtetiKojePohadjaComponent implements OnInit {

  @Input() predmeti: Predmet[]=[];
  constructor() { }

  ngOnInit() {
  }

}
