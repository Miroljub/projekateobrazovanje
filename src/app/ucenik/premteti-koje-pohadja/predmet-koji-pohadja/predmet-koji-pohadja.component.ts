import { Component, OnInit, Input } from '@angular/core';
import { Predmet } from 'src/app/predmeti/predmet.model';

@Component({
  selector: 'app-predmet-koji-pohadja',
  templateUrl: './predmet-koji-pohadja.component.html',
  styleUrls: ['./predmet-koji-pohadja.component.css']
})
export class PredmetKojiPohadjaComponent implements OnInit {

  @Input() index: number;
  @Input() predmet: Predmet;

  constructor() { }

  ngOnInit() {
  }

}
