import { Component, OnInit } from '@angular/core';
import { Predmet } from 'src/app/predmeti/predmet.model';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { PredmetService } from 'src/app/predmeti/predmet.service';

@Component({
  selector: 'app-predmet-koji-pohadja-detalji',
  templateUrl: './predmet-koji-pohadja-detalji.component.html',
  styleUrls: ['./predmet-koji-pohadja-detalji.component.css']
})
export class PredmetKojiPohadjaDetaljiComponent implements OnInit {

  predmet: Predmet;
  id: number;

  constructor(private predmetService: PredmetService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.route.params
    .subscribe(
      (params: Params) => {
        this.id = +params['id'];
        this.predmet = this.predmetService.getPredmet(this.id);
      }
    );
  }

}
