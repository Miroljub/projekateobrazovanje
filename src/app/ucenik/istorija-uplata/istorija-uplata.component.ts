import { Component, OnInit } from '@angular/core';
import { UceniciService } from 'src/app/ucenici/ucenici.service';
import { Uplate } from 'src/app/uplate/uplate.model';

@Component({
  selector: 'app-istorija-uplata',
  templateUrl: './istorija-uplata.component.html',
  styleUrls: ['./istorija-uplata.component.css']
})
export class IstorijaUplataComponent implements OnInit {

  id: number;
  uplate: Uplate[];

  constructor(private uceniciService: UceniciService) { }

  ngOnInit() {
    this.id = +localStorage.getItem('id');
    this.uceniciService.getUcenikIstorijaUplata(this.id).subscribe(
      (uplate: Uplate[]) => {
        this.uplate = uplate;
        console.log(uplate);
      }
    );
  }


}
