import { Component, OnInit } from '@angular/core';
import { UceniciService } from 'src/app/ucenici/ucenici.service';
import { Predmet } from 'src/app/predmeti/predmet.model';

@Component({
  selector: 'app-lista-nepolozenih-predmeta',
  templateUrl: './lista-nepolozenih-predmeta.component.html',
  styleUrls: ['./lista-nepolozenih-predmeta.component.css']
})
export class ListaNepolozenihPredmetaComponent implements OnInit {

  id: number;
  predmeti: Predmet[];

  constructor(private uceniciService: UceniciService) { }

  ngOnInit() {
    this.id = +localStorage.getItem('id');
    this.uceniciService.getNepolozeniPredmetiBaza(this.id).subscribe(
      (predmeti: Predmet[]) => {
        this.predmeti = predmeti;
        console.log(predmeti);
      }
    );
  }

}
