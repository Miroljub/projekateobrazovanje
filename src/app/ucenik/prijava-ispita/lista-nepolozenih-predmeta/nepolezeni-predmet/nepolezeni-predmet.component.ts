import { Component, OnInit, Input } from '@angular/core';
import { Predmet } from 'src/app/predmeti/predmet.model';

@Component({
  selector: 'app-nepolezeni-predmet',
  templateUrl: './nepolezeni-predmet.component.html',
  styleUrls: ['./nepolezeni-predmet.component.css']
})
export class NepolezeniPredmetComponent implements OnInit {

  @Input() predmet: Predmet;
  @Input() id:number
  constructor() { }

  ngOnInit() {
  }

}
