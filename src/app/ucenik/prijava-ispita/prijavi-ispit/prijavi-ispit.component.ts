import { Component, OnInit } from '@angular/core';
import { PredmetService } from 'src/app/predmeti/predmet.service';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { Predmet } from 'src/app/predmeti/predmet.model';
import { Ispit } from 'src/app/models/ispit';
import { UceniciService } from 'src/app/ucenici/ucenici.service';
import { Ucenik } from 'src/app/ucenici/ucenici.model';

@Component({
  selector: 'app-prijavi-ispit',
  templateUrl: './prijavi-ispit.component.html',
  styleUrls: ['./prijavi-ispit.component.css']
})
export class PrijaviIspitComponent implements OnInit {

  predmet:Predmet = new Predmet(null,'',null,[],[]);
  id:number;
  rokovi:Ispit[]=[];
  idUcenik: number;
  ucenik: Ucenik;
  constructor(private ucenikService: UceniciService, private predmetService:PredmetService,private route: ActivatedRoute, private router: Router) { }


  public selectedId:number;
  
  public highlightRow(emp: Ispit) {
    this.selectedId = emp.id;
    console.log(this.selectedId)
  }

  ngOnInit() {
    this.route.params
    .subscribe(
      (params: Params) => {
        this.id = +params['id'];
        console.log(this.id);
        this.predmet = this.predmetService.getPredmet(this.id);
        this.predmetService.getNePrijavljenjeIspite(this.id,+localStorage.getItem('id')).subscribe(
          (ispiti: Ispit[])=>{
            this.rokovi = ispiti;
          }
        )
      }
    );    
    
  }

  prijavi(){
    this.ucenikService.prijaviIspitBaza(this.selectedId, +localStorage.getItem('id')).subscribe(
      data => {this.rokovi = this.rokovi.filter(item => item.id != this.selectedId);}
    );

    this.idUcenik = +localStorage.getItem('id');
    this.ucenik = this.ucenikService.getUcenik(this.idUcenik);
    let novoStanje: number = this.ucenik.stanjeRacun - 200;
    this.ucenik.stanjeRacun = novoStanje;
    const ucenikNovi: Ucenik = new Ucenik(this.ucenik.id, this.ucenik.username, this.ucenik.password, this.ucenik.ime, this.ucenik.prezime,
                                          this.ucenik.datumRodjenja,this.ucenik.mestoRodjenja, this.ucenik.brojIndeksa, this.ucenik.studijskiProgram
                                          ,this.ucenik.racun, this.ucenik.tipStudija, this.ucenik.uplate, novoStanje)
    this.ucenikService.updateBazuUcenik(ucenikNovi).subscribe(
      (ucenik:Ucenik) =>{
        this.ucenikService.updateUcenik(ucenik);
         }
       );
    }

}
