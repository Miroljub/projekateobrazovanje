import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { UceniciService } from 'src/app/ucenici/ucenici.service';
import { Ucenik } from 'src/app/ucenici/ucenici.model';

@Component({
  selector: 'app-ucenik-uplatnica',
  templateUrl: './ucenik-uplatnica.component.html',
  styleUrls: ['./ucenik-uplatnica.component.css']
})
export class UcenikUplatnicaComponent implements OnInit {

  isShown: boolean = false;
  ucenik: Ucenik=<Ucenik>{};
  id: number;
  value: number = 0;

  constructor(private route: ActivatedRoute, private router: Router, private uceniciService: UceniciService) { }

  ngOnInit() {
    
    this.route.params
    .subscribe(
      (params: Params) => {
        this.id = +params['id'];
        this.uceniciService.getUCenikBaza(this.id).subscribe(
          (ucenik: Ucenik) => {this.ucenik = ucenik}
        );
        console.log(this.ucenik)
      }     
    );

    
    this.value = 0;
  }

  toggleShow() {
    this.isShown = ! this.isShown;
    }

    onOdustani() {
      //this.router.navigate(['../'], {relativeTo: this.route});
      this.isShown = ! this.isShown;
    }
    onSubmit() {
      this.uceniciService.uplataNaRacunBaza(this.id, this.value).subscribe(
        data => {
          this.uceniciService.getUCenikBaza(this.id).subscribe(
            (ucenik: Ucenik) => {this.ucenik = ucenik}
          );
        }
      );
      this.isShown = ! this.isShown;
      this.value = 0;
    }

}
