import { Component, OnInit } from '@angular/core';
import { UceniciService } from './ucenici.service';
import { Ucenik } from './ucenici.model';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-ucenici',
  templateUrl: './ucenici.component.html',
  styleUrls: ['./ucenici.component.css']
})
export class UceniciComponent implements OnInit {

  ucenici: Ucenik[];

  constructor(private httpClient:HttpClient, private uceniciServis: UceniciService,
              private router:Router) { }

  ngOnInit() {
    if(localStorage.getItem('role') == 'ADMIN'){
      this.uceniciServis.getUceniciBaza().subscribe(
        (ucenici: Ucenik[]) => {
          this.uceniciServis.setUcenik(ucenici)
        });  
    }else{
        this.router.navigate([''])
    }

    
  };

}
