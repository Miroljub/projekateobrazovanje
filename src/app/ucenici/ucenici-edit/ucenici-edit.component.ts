import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { UceniciService } from '../ucenici.service';
import { Ucenik } from '../ucenici.model';
import { HttpClient, HttpHeaders } from '@angular/common/http';


@Component({
  selector: 'app-ucenici-edit',
  templateUrl: './ucenici-edit.component.html',
  styleUrls: ['./ucenici-edit.component.css']
})
export class UceniciEditComponent implements OnInit {
  id: number;
  editMode = false;
  uceniciForm: FormGroup;
  fileToUpload:any;
  formData = new FormData(); 
  files:any[]=[];




  @ViewChild('myInput') myInputVariable: ElementRef;

  constructor(private route: ActivatedRoute, private uceniciServis: UceniciService, private router: Router,
              private http: HttpClient) { }

  ngOnInit() {
    this.route.params
      .subscribe(
        (params: Params) => {
          this.id = +params['id'];
          this.editMode = params['id'] != null;
          this.initForm();
        }
      );
  }

  tipStudijaLista = ['OAS', 'OSS'];

  onSubmit() {
    if (this.editMode) {
      console.log(this.uceniciForm.value.stanjeRacun)
      const ucenikE:Ucenik = new Ucenik(this.id, this.uceniciForm.value.username, this.uceniciForm.value.password, this.uceniciForm.value.ime,this.uceniciForm.value.prezime,
                                            this.uceniciForm.value.datumRodjenja,this.uceniciForm.value.mestoRodjenja,this.uceniciForm.value.brojIndeksa,
                                            this.uceniciForm.value.studijskiProgram, this.uceniciForm.value.racun, this.uceniciForm.value.tipStudija,[],this.uceniciForm.value.stanjeRacun)
      this.uceniciServis.updateBazuUcenik(ucenikE).subscribe(
        (ucenik:Ucenik) =>{
          this.uceniciServis.updateUcenik(ucenik);
        }
      )
      
    } else {
      console.log(this.uceniciForm.value)
      this.uceniciServis.addUBazuUcenika(this.uceniciForm.value).subscribe(
        (ucenik: Ucenik) => {
          console.log(this.files)
          for(let file of this.files)
          {
            const formData = new FormData();
            formData.append('file',file, file.name)
            formData.append('naslov',file.name)
            formData.append('ucenik',ucenik.id.toString())

            this.uceniciServis.dodajDokumentaPriPravljenju(formData).subscribe(
              data => console.log(data)
            )
          }

          console.log(ucenik)
          this.uceniciServis.addUcenik(ucenik)
        }
      );
     
    }
    this.onOdustani();
  }

    onOdustani() {
      this.router.navigate(['../'], {relativeTo: this.route});
    }
    


  private initForm() {
    let ucenikIme = '';
    let ucenikPrezime = '';
    let ucenikDatumRodjenja = '';
    let ucenikMestoRodjenja = '';
    let ucenikBrojIndeksa = '';
    let ucenikStudijskiProgram = '';
    let ucenikRacun = '';
    let ucenikTipStudija = '';
    let ucenikUsername = '';
    let ucenikPassword = '';
    let ucenikStanjeRacun = 0;

    if (this.editMode) {
      const ucenik = this.uceniciServis.getUcenik(this.id);
      this.uceniciServis.downalodFiles(this.id).subscribe(
        (data: any) => {
          for(let d of data){
            this.files.push(d);
          }
          console.log(this.files)
        }
      )
      ucenikIme = ucenik.ime;
      ucenikPrezime = ucenik.prezime;
      ucenikDatumRodjenja = ucenik.datumRodjenja;
      ucenikMestoRodjenja = ucenik.mestoRodjenja;
      ucenikBrojIndeksa = ucenik.brojIndeksa;
      ucenikStudijskiProgram = ucenik.studijskiProgram;
      ucenikRacun = ucenik.racun;
      ucenikTipStudija = ucenik.tipStudija;
      ucenikUsername = ucenik.username;
      ucenikPassword = ucenik.password;
      ucenikStanjeRacun = ucenik.stanjeRacun;
    }



    this.uceniciForm = new FormGroup({
      'ime': new FormControl(ucenikIme, Validators.required),
      'prezime': new FormControl(ucenikPrezime, Validators.required),
      'datumRodjenja': new FormControl(ucenikDatumRodjenja, Validators.required),
      'mestoRodjenja': new FormControl(ucenikMestoRodjenja, Validators.required),
      'brojIndeksa': new FormControl(ucenikBrojIndeksa, Validators.required),
      'studijskiProgram': new FormControl(ucenikStudijskiProgram, Validators.required),
      'racun': new FormControl(ucenikRacun, Validators.required),
      'tipStudija': new FormControl(ucenikTipStudija, Validators.required),
      'username': new FormControl(ucenikUsername, Validators.required),
      'password': new FormControl(ucenikPassword, Validators.required),
      'stanjeRacun': new FormControl(ucenikStanjeRacun)
    });
  }

  postMethod(files: FileList) {
    const  httpOptions = {
      headers: new HttpHeaders({
        'Authorization': localStorage.getItem('token')
      })
    };
    // this.fileToUpload = files.item(0); 
    // this.formData.append('file',this.fileToUpload, this.fileToUpload.name); 
    // this.formData.append('naslov',"Pera");
    // this.formData.append('ucenik','1');
    
    
    for(let file of this.files)
    {
      if(file.name == files.item(0).name){
        this.myInputVariable.nativeElement.value = "";
        return;
      }      
    }
    this.files.push(files.item(0));

    this.myInputVariable.nativeElement.value = "";
    console.log(files)
    console.log(this.files)
    // this.http.post('http://localhost:8080/api/dokumenta/upload', this.formData ,httpOptions).subscribe((val) => {
    
    // console.log(val);
    // });
    // return false; 
}

Delete(file:any)
{
  this.files = this.files.filter(item => item.name != file.name);
}

pogledajPdf(file:any)
{
  console.log("asd")
  this.uceniciServis.pogledaj(file.id).subscribe(data => data);
}


openNewTab(path:string)
{
  console.log(path)
  const a : string = path.substr(1);
  window.open("C"+a, "_blank");
}

}
