import { Uplate } from '../uplate/uplate.model';

export class Ucenik {
    constructor(
        public id:number,
        public username:string,
        public password:string,
        public ime: string,
        public prezime: string,
        public datumRodjenja: string,
        public mestoRodjenja: string,
        public brojIndeksa: string,
        public studijskiProgram: string,
        public racun: string,
        public tipStudija: string,
        public uplate: Uplate[],
        public stanjeRacun: number
    ){}
    // public id:number;
    // public ime: string;
    // public prezime: string;
    // public datumRodjenja: string;
    // public mestoRodjenja: string;
    // public brojIndeksa: string;
    // public studijskiProgram: string;
    // public racun: number;
    // public tipStudija: string;
    // public uplate: Uplate[];

    // constructor(id:number, ime: string, prezime: string, datumRodjenja: string, mestoRodjenja: string,
    //             brojIndeksa: string, studijskiProgram: string, racun: number, tipStudija: string, uplate: Uplate[]) {
    //     this.id = id;
    //     this.ime = ime;
    //     this.prezime = prezime;
    //     this.datumRodjenja = datumRodjenja;
    //     this.mestoRodjenja = mestoRodjenja;
    //     this.brojIndeksa = brojIndeksa;
    //     this.studijskiProgram = studijskiProgram;
    //     this.racun = racun;
    //     this.tipStudija = tipStudija;
    //     this.uplate = uplate;
    // }
    
}