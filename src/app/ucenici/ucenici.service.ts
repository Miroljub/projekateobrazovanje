
import { Ucenik } from './ucenici.model';
import { Subject } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Uplate } from '../uplate/uplate.model';
import { IzmenaRacuna } from '../models/izmenaRacuna.model';
import { Predmet } from '../predmeti/predmet.model';

@Injectable()
export class UceniciService {
    selektovaniUcenik = new Subject<Ucenik>();
    ucenikChanged = new Subject<Ucenik[]>();
    id:number;
     httpOptions = {
        headers: new HttpHeaders({
          'Content-Type':  'application/json',
          'Authorization': localStorage.getItem('token')
        })
      };

    constructor(private httpClient: HttpClient){} 

    private ucenici: Ucenik[] = [
        // new Ucenik(0, 'Miro', 'Miric', '5.5.2010', 'Novi Sad', 'sa78/2019', 'FTN', 'ads787'),
        // new Ucenik(1, 'Pero', 'Peric', '10.7.2010', 'Bijeljina', 'sa8/2019', 'FTN', 'ads787'),
        // new Ucenik(12, 'Milka', 'Milkic', '9.9.2010', 'Bijeljina', 'sa8/2019', 'FTN', 'ads787')
    ];

    getUcenici() {
        return this.ucenici.slice();
    }

    getUcenik(index: number) {
        for(let ucenik of this.ucenici) {
            if (ucenik.id == index) {
                return ucenik;
            }
        }
    }

    getUCenikBaza(id: number) {
      const httpOptions = {
          headers: new HttpHeaders({
            'Content-Type':  'application/json',
            'Authorization': localStorage.getItem('token')
          })
        };
      return this.httpClient.get<Ucenik>(`http://localhost:8080/api/ucenici/${id}`, httpOptions);
  }


    getPredmetiKojePohadjaUcenikBaza(id: number) {
        const httpOptions = {
            headers: new HttpHeaders({
              'Content-Type':  'application/json',
              'Authorization': localStorage.getItem('token')
            })
          };
        return this.httpClient.get<Predmet[]>(`http://localhost:8080/api/ucenici/${id}/pohadja`, httpOptions);
    }

    getPolozeniPredmetiBaza(id: number) {
        const httpOptions = {
            headers: new HttpHeaders({
              'Content-Type':  'application/json',
              'Authorization': localStorage.getItem('token')
            })
          };
        return this.httpClient.get<Predmet[]>(`http://localhost:8080/api/ucenici/${id}/pohadja/polozeni`,httpOptions);
    }
    getNepolozeniPredmetiBaza(id: number) {
        const httpOptions = {
            headers: new HttpHeaders({
              'Content-Type':  'application/json',
              'Authorization': localStorage.getItem('token')
            })
          };
        return this.httpClient.get<Predmet[]>(`http://localhost:8080/api/ucenici/${id}/pohadja/nepolozeni`,httpOptions);
    }

    getUcenikIstorijaUplata(id: number) {
        const httpOptions = {
            headers: new HttpHeaders({
              'Content-Type':  'application/json',
              'Authorization': localStorage.getItem('token')
            })
          };
        return this.httpClient.get<Uplate[]>(`http://localhost:8080/api/ucenici/${id}/uplate`,httpOptions);
    }

    prijaviIspitBaza(ispitId: number, ucenikId:number){
        const httpOptions = {
            headers: new HttpHeaders({
              'Content-Type':  'application/json',
              'Authorization': localStorage.getItem('token')
            })
          };
        return this.httpClient.post<any>(`http://localhost:8080/api/ispiti/${ispitId}/prijave/${ucenikId}`,{}, httpOptions);
    }

    updateUcenik(ucenikE: Ucenik) {
        for(let ucenik of this.ucenici){
            if(ucenik.id == ucenikE.id){
                console.log(ucenikE.stanjeRacun +" iz servisa")
                ucenik.ime = ucenikE.ime;
                ucenik.prezime = ucenikE.prezime;
                ucenik.datumRodjenja = ucenikE.datumRodjenja;
                ucenik.mestoRodjenja = ucenikE.mestoRodjenja;
                ucenik.brojIndeksa = ucenikE.brojIndeksa;
                ucenik.studijskiProgram = ucenikE.studijskiProgram;
                ucenik.racun = ucenikE.racun;
                ucenik.tipStudija = ucenikE.tipStudija;
                ucenik.username = ucenikE.username;
                ucenik.password = ucenikE.password;
                ucenik.stanjeRacun = ucenikE.stanjeRacun;
        
            }
       } 
        this.ucenikChanged.next(this.ucenici.slice());
    }
    updateBazuUcenik(ucenik:Ucenik){
        return this.httpClient.put<Ucenik>('http://localhost:8080/api/ucenici',ucenik,this.httpOptions);
    }

    uplataNaRacunBaza(id:number, uplata:number){
        const httpOptions = {
            headers: new HttpHeaders({
              'Content-Type':  'application/json',
              'Authorization': localStorage.getItem('token')
            })
          };
        const izmenaRacuna: IzmenaRacuna = new IzmenaRacuna(uplata);
        return this.httpClient.put(`http://localhost:8080/api/ucenici/${id}/racun`, izmenaRacuna, httpOptions);
    }

    uplataNaRacun(id: number) {
        const httpOptions = {
            headers: new HttpHeaders({
              'Content-Type':  'application/json',
              'Authorization': localStorage.getItem('token')
            })
          };
        return this.httpClient.get<Ucenik>(`http://localhost:8080/api/ucenici/${id}`,httpOptions).subscribe(
            (ucenikE: Ucenik ) => {
                for(let ucenik of this.ucenici){
                    if(ucenik.id == ucenikE.id){
                        console.log(ucenikE);
                        ucenik.stanjeRacun = ucenikE.stanjeRacun;
                        ucenik.uplate = ucenikE.uplate
                    }
               } 
               console.log(this.ucenici)
                this.ucenikChanged.next(this.ucenici.slice());
            }
        );
    }

    setUcenik(ucenici: Ucenik[]){
        this.ucenici = ucenici;
        this.ucenikChanged.next(this.ucenici.slice());
    }

    deleteUcenik(id: number) {
        var index = this.ucenici.map(x => {
            return x.id;
          }).indexOf(id);

        this.ucenici.splice(index,1)

        this.ucenikChanged.next(this.ucenici.slice());
    } 
    deleteUcenikIzBaze(id:number){
        return this.httpClient.delete(`http://localhost:8080/api/ucenici/${id}`, this.httpOptions);
    } 
    addUcenik(newUcenik: Ucenik) {
        
        this.ucenici.push(newUcenik);
        this.id = null;
        this.ucenikChanged.next(this.ucenici.slice());
    }

    addUBazuUcenika(newUcenik: Ucenik){
      
        return this.httpClient.post<Ucenik>('http://localhost:8080/api/ucenici',newUcenik,this.httpOptions);
    }

    getUceniciBaza() {
        const  httpOptions = {
            headers: new HttpHeaders({
              'Content-Type':  'application/json',
              'Authorization': localStorage.getItem('token')
            })
          };
        return this.httpClient.get<Ucenik[]>('http://localhost:8080/api/ucenici',httpOptions);
    }

    dodajDokumentaPriPravljenju( formData :FormData)
    {
      const  httpOptions = {
        headers: new HttpHeaders({
          'Authorization': localStorage.getItem('token')
        })
      };

      
      return this.httpClient.post('http://localhost:8080/api/dokumenta/upload', formData ,httpOptions)

    }

    downalodFiles(id:number)
    {
      const  httpOptions = {
        headers: new HttpHeaders({
          'Authorization': localStorage.getItem('token')
        })
      };

      return this.httpClient.get<any>(`http://localhost:8080/api/ucenici/${id}/dokumenta`,httpOptions);

    }

    pogledaj(id:number)
    {
      const  httpOptions = {
        headers: new HttpHeaders({
          'Authorization': localStorage.getItem('token')
        })
      };

      return this.httpClient.get<any>(`http://localhost:8080/api/dokumenta/download/${id}`,httpOptions);

    }
    
}