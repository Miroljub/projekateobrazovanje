import { Component, OnInit, Input } from '@angular/core';
import { Ucenik } from '../../ucenici.model';

@Component({
  selector: 'app-ucenici-item',
  templateUrl: './ucenici-item.component.html',
  styleUrls: ['./ucenici-item.component.css']
})
export class UceniciItemComponent implements OnInit {
  @Input() ucenik: Ucenik;
  @Input() index: number;

  ngOnInit() {
  }

}
