import { Component, OnInit } from '@angular/core';
import { Ucenik } from '../ucenici.model';
import { UceniciService } from '../ucenici.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-ucenici-lista',
  templateUrl: './ucenici-lista.component.html',
  styleUrls: ['./ucenici-lista.component.css']
})
export class UceniciListaComponent implements OnInit {

  ucenici: Ucenik[];

  constructor( private uceniciService: UceniciService, private router: Router, private route: ActivatedRoute ) { }

  ngOnInit() {
    this.uceniciService.ucenikChanged
      .subscribe(
        (ucenici: Ucenik[]) => {
          console.log(ucenici)
          this.ucenici = ucenici;
        }
      );
    this.ucenici = this.uceniciService.getUcenici();
  }

  onNoviUcenik() {
    this.router.navigate(['new'], {relativeTo: this.route});
  }

}
