import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { UceniciService } from '../../ucenici.service';
import { Ucenik } from '../../ucenici.model';

@Component({
  selector: 'app-ucenik-podaci-edit',
  templateUrl: './ucenik-podaci-edit.component.html',
  styleUrls: ['./ucenik-podaci-edit.component.css']
})
export class UcenikPodaciEditComponent implements OnInit {

  id: number;
  uceniciForm: FormGroup;

  constructor(private route: ActivatedRoute, private uceniciService: UceniciService, private router: Router ) { }

  ngOnInit() {
    this.route.params
      .subscribe(
        (params: Params) => {
          this.id = +params['id'];
          this.initForm();
        }
      );
      
  }

  tipStudijaLista = ['OAS', 'OSS'];

  onSubmit() {
    const ucenikE:Ucenik = new Ucenik(this.id, this.uceniciForm.value.username, this.uceniciForm.value.password, this.uceniciForm.value.ime,this.uceniciForm.value.prezime,
      this.uceniciForm.value.datumRodjenja,this.uceniciForm.value.mestoRodjenja,this.uceniciForm.value.brojIndeksa,
      this.uceniciForm.value.studijskiProgram, this.uceniciForm.value.racun, this.uceniciForm.value.tipStudija,[], this.uceniciForm.value.stanjeRacun)
         this.uceniciService.updateBazuUcenik(ucenikE).subscribe(
                (ucenik:Ucenik) =>{
                this.uceniciService.updateUcenik(ucenik);
              }
          )
          this.onOdustani();
    }
    onOdustani() {
      this.router.navigate(['../'], {relativeTo: this.route});
    }

    private initForm() {
      let ucenikIme = '';
      let ucenikPrezime = '';
      let ucenikDatumRodjenja = '';
      let ucenikMestoRodjenja = '';
      let ucenikBrojIndeksa = '';
      let ucenikStudijskiProgram = '';
      let ucenikRacun = '';
      let ucenikTipStudija = '';
      let ucenikUsername = '';
      let ucenikPassword = '';
      let ucenikStanjeRacun = 0;
  
    
        const ucenik = this.uceniciService.getUcenik(this.id);
        ucenikIme = ucenik.ime;
        ucenikPrezime = ucenik.prezime;
        ucenikDatumRodjenja = ucenik.datumRodjenja;
        ucenikMestoRodjenja = ucenik.mestoRodjenja;
        ucenikBrojIndeksa = ucenik.brojIndeksa;
        ucenikStudijskiProgram = ucenik.studijskiProgram;
        ucenikRacun = ucenik.racun;
        ucenikTipStudija = ucenik.tipStudija;
        ucenikUsername = ucenik.username;
        ucenikPassword = ucenik.password;
        ucenikStanjeRacun = ucenik.stanjeRacun;
  
  
      this.uceniciForm = new FormGroup({
        'ime': new FormControl(ucenikIme, Validators.required),
        'prezime': new FormControl(ucenikPrezime, Validators.required),
        'datumRodjenja': new FormControl(ucenikDatumRodjenja, Validators.required),
        'mestoRodjenja': new FormControl(ucenikMestoRodjenja, Validators.required),
        'brojIndeksa': new FormControl(ucenikBrojIndeksa, Validators.required),
        'studijskiProgram': new FormControl(ucenikStudijskiProgram, Validators.required),
        'racun': new FormControl(ucenikRacun, Validators.required),
        'tipStudija': new FormControl(ucenikTipStudija, Validators.required),
        'username': new FormControl(ucenikUsername, Validators.required),
        'password': new FormControl(ucenikPassword, Validators.required),
        'stanjeRacun': new FormControl(ucenikStanjeRacun)
      });
    }

}
