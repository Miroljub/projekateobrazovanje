import { Component, OnInit } from '@angular/core';
import { Ucenik } from '../ucenici.model';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { UceniciService } from '../ucenici.service';

@Component({
  selector: 'app-ucenik-licni-podaci',
  templateUrl: './ucenik-licni-podaci.component.html',
  styleUrls: ['./ucenik-licni-podaci.component.css']
})
export class UcenikLicniPodaciComponent implements OnInit {
  ucenik: Ucenik=<Ucenik>{};
  id: number;
  files:any[] = [];

  constructor(private uceniciServis: UceniciService, private route: ActivatedRoute, private router: Router) {}

  ngOnInit() {
    this.route.params
      .subscribe(
        (params: Params) => {          
          this.id = +params['id'];
          this.uceniciServis.getUCenikBaza(this.id).subscribe(
            (ucenik: Ucenik) => {this.ucenik = ucenik}
          );
          this.uceniciServis.downalodFiles(this.id).subscribe(
            (data: any) => {
              for(let d of data){
                this.files.push(d);
              }
              console.log(this.files)
            }
          )
        }
      );
      this.uceniciServis.ucenikChanged
      .subscribe(
        (ucenici: Ucenik[]) => {
          for(let ucenik of ucenici){
            if(ucenik.id === this.id){
              this.ucenik = ucenik
            }
          }
        }
      );
    
    
  }

  onIzmeniPodatke(){
    this.router.navigate(['edit'], {relativeTo: this.route});
  }

}
