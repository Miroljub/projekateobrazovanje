import { Component, OnInit } from '@angular/core';
import { Ucenik } from '../ucenici.model';
import { UceniciService } from '../ucenici.service';
import { ActivatedRoute, Params, Router } from '@angular/router';

@Component({
  selector: 'app-ucenici-detalji',
  templateUrl: './ucenici-detalji.component.html',
  styleUrls: ['./ucenici-detalji.component.css']
})
export class UceniciDetaljiComponent implements OnInit {
  ucenik: Ucenik;
  id: number;

  constructor(private uceniciServis: UceniciService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.route.params
      .subscribe(
        (params: Params) => {
          this.id = +params['id'];
          this.ucenik = this.uceniciServis.getUcenik(this.id);
        }
      );
  }

  onIzmeniUcenika() {
    this.router.navigate(['edit'], {relativeTo: this.route});
  }

  onIzbrisiUcenika(){
    this.uceniciServis.deleteUcenikIzBaze(this.id).subscribe(
      data => {
        this.uceniciServis.deleteUcenik(this.id); 
      }
    );
      
    this.router.navigate(['/ucenici']);
  }

  }

