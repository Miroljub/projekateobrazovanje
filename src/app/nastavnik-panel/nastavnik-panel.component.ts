import { Component, OnInit } from '@angular/core';
import { Nastavnik } from '../nastavnici/nastavnik.model';
import { NastavnikService } from '../nastavnici/nastavnik.service';

@Component({
  selector: 'app-nastavnik-panel',
  templateUrl: './nastavnik-panel.component.html',
  styleUrls: ['./nastavnik-panel.component.css']
})
export class NastavnikPanelComponent implements OnInit {

  nastavnik: Nastavnik;
  constructor(private nastavnikServis: NastavnikService) { 
    this.nastavnikServis.getNastavnikBaza(+localStorage.getItem('id')).subscribe(
      (nastavnik: Nastavnik) => {
        this.nastavnik = nastavnik
      }
    )
  }

  ngOnInit() {
    console.log("asd")
    if(localStorage.getItem('role') == 'NASTAVNIK'){ 
      
      
    }

  }
  

}
