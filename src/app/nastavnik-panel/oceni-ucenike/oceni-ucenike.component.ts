import { Component, OnInit } from '@angular/core';
import { Ucenik } from 'src/app/ucenici/ucenici.model';
import { PredmetService } from 'src/app/predmeti/predmet.service';
import { ActivatedRoute, Router, Params } from '@angular/router';

@Component({
  selector: 'app-oceni-ucenike',
  templateUrl: './oceni-ucenike.component.html',
  styleUrls: ['./oceni-ucenike.component.css']
})
export class OceniUcenikeComponent implements OnInit {
  id:number;
  ucenici:Ucenik[]=[];
  constructor(private predmetService:PredmetService,private route: ActivatedRoute, private router: Router) { }

  public selectedId:number;
  
  public highlightRow(emp: Ucenik) {
    this.selectedId = emp.id;
    console.log(this.selectedId)
  }


  ngOnInit() {
    this.route.params
    .subscribe(
      (params: Params) => {
        this.id = +params['id'];
      }
    );
    this.predmetService.getUcenikeKojiNisuPoloziliPredmet(this.id).subscribe(
      (ucenici:Ucenik[]) => {
        this.ucenici = ucenici;
      }
    )

  }

  oceniUcenika(){
    this.router.navigate([`oceni/${this.selectedId}`], {relativeTo: this.route});
  }
 

}
