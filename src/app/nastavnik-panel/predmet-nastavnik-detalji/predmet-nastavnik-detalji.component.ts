import { Component, OnInit } from '@angular/core';
import { Predmet } from 'src/app/predmeti/predmet.model';
import { Route, Params, ActivatedRoute, Router } from '@angular/router';
import { PredmetService } from 'src/app/predmeti/predmet.service';
import { Ispit } from 'src/app/models/ispit';

@Component({
  selector: 'app-predmet-nastavnik-detalji',
  templateUrl: './predmet-nastavnik-detalji.component.html',
  styleUrls: ['./predmet-nastavnik-detalji.component.css']
})
export class PredmetNastavnikDetaljiComponent implements OnInit {
  predmet:Predmet = new Predmet(null,'',null,[],[]);
  id:number;
  rokovi:Ispit[]=[];
  constructor(private predmetService:PredmetService,private route: ActivatedRoute, private router: Router) { }


  public selectedId:number;
  
  public highlightRow(emp: Ispit) {
    this.selectedId = emp.id;
    console.log(this.selectedId)
  }


  ngOnInit() {
    
    this.route.params
    .subscribe(
      (params: Params) => {
        this.id = +params['id'];
        this.predmet = this.predmetService.getPredmet(this.id);
        this.predmetService.getPostojeciIspiti(this.id).subscribe(
          (ispiti: Ispit[])=>{
            console.log(ispiti)
            this.rokovi = ispiti
          }
        )
      }
    ); 
      
    
  }

  onDodajRok(){
    this.router.navigate(['rok'], {relativeTo: this.route});
  }

  DeleteR(id:number){
    this.predmetService.izbrisiRok(id).subscribe();
    this.rokovi = this.rokovi.filter(item => item.id != id);
  }

  oceni(){
    this.router.navigate([`ispiti/${this.selectedId}`], {relativeTo: this.route});
  }

  oceniUcenike(){
    this.router.navigate([`ucenici`], {relativeTo: this.route});
  }

}
