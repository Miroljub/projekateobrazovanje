import { Component, OnInit } from '@angular/core';
import { Predmet } from 'src/app/predmeti/predmet.model';
import { PredmetService } from 'src/app/predmeti/predmet.service';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { Ispit } from 'src/app/models/ispit';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-rok',
  templateUrl: './rok.component.html',
  styleUrls: ['./rok.component.css']
})
export class RokComponent implements OnInit {
  rokForm: FormGroup;
  predmet:Predmet;
  id:number;
  postojeci_rokovi: Ispit[]=[];
  tipovi:string[]=['ISPIT','KOLOKVIJUM','PROJEKAT','OSTALO'];
  rokovi:string[] = ['JANUARKSI','FEBRUARSKI','APRILSKI','JUNSKI','JULSKI','AVGUSTOVSKI','SEPTEMBARSKI','OKTOBARSKI'];
  constructor(private predmetService:PredmetService,private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.route.params
    .subscribe(
      (params: Params) => {
        this.id = +params['id'];
        this.initForm();
        this.predmet = this.predmetService.getPredmet(this.id);
        this.predmetService.getPostojeciIspiti(this.id).subscribe(
          (ispiti: Ispit[])=>{this.postojeci_rokovi = ispiti}         
        )
      }
    );   
  }

  private initForm() {
    let tipIspita = '';
    let ispitniRok = '';
    let maksimumBodova = 0;



    this.rokForm = new FormGroup({
      'tipIspita': new FormControl(tipIspita, Validators.required),
      'ispitniRok': new FormControl(ispitniRok, Validators.required),
      'maksimumBodova': new FormControl(maksimumBodova, Validators.required)
    });
  }

  onSubmit(){
    
    const ispit:Ispit= new Ispit(null,this.rokForm.value.ispitniRok,
        this.id,this.rokForm.value.maksimumBodova,this.rokForm.value.tipIspita,[]);

        this.predmetService.dodajRok(ispit).subscribe(
          data =>this.postojeci_rokovi.push(ispit)
        )

        this.rokForm.reset();


  }

  onOdustani(){
    this.router.navigate(['../'], {relativeTo: this.route});
  }

  

}
