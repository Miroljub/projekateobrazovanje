import { Component, OnInit, Input, OnChanges } from '@angular/core';
import { Predmet } from 'src/app/predmeti/predmet.model';
import { Nastavnik } from 'src/app/nastavnici/nastavnik.model';
import { NastavnikService } from 'src/app/nastavnici/nastavnik.service';
import { PredmetService } from 'src/app/predmeti/predmet.service';
import { Predaje } from 'src/app/models/predaje.model';

@Component({
  selector: 'app-lista-predmeta-nastavnik',
  templateUrl: './lista-predmeta-nastavnik.component.html',
  styleUrls: ['./lista-predmeta-nastavnik.component.css']
})
export class ListaPredmetaNastavnikComponent implements OnInit,OnChanges {
  predaje: Predaje[];
  predmeti: Predmet[] = [];
  
  public searchText : string;
  @Input() nastavnik: Nastavnik;

  constructor(private nastavnikServis: NastavnikService) { }

 

  ngOnChanges(){
    
  }

  ngOnInit() {
    this.nastavnikServis.getPredajeBaza(+localStorage.getItem('id')).subscribe(
      (predaje: Predaje[]) => {
        this.predaje = predaje;
      }
    )
  }
}
