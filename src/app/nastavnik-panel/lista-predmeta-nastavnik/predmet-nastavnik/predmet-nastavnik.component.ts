import { Component, OnInit, Input } from '@angular/core';
import { Predaje } from 'src/app/models/predaje.model';

@Component({
  selector: 'app-predmet-nastavnik',
  templateUrl: './predmet-nastavnik.component.html',
  styleUrls: ['./predmet-nastavnik.component.css']
})
export class PredmetNastavnikComponent implements OnInit {
  @Input() predaje: Predaje;
  @Input() id: number;
  constructor() { }

  ngOnInit() {
    console.log(this.predaje)
  }

}
