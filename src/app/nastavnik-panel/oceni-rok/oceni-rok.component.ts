import { Component, OnInit } from '@angular/core';
import { PredmetService } from 'src/app/predmeti/predmet.service';
import { Params, ActivatedRoute, Router } from '@angular/router';
import { Ucenik } from 'src/app/ucenici/ucenici.model';
import { UceniciService } from 'src/app/ucenici/ucenici.service';
import { Prijava } from 'src/app/models/prijava';
import { Ispit } from 'src/app/models/ispit';
import { Predmet } from 'src/app/predmeti/predmet.model';
import { PrijavaUcenik } from 'src/app/models/prijava-ucenik';

@Component({
  selector: 'app-oceni-rok',
  templateUrl: './oceni-rok.component.html',
  styleUrls: ['./oceni-rok.component.css']
})
export class OceniRokComponent implements OnInit {
  predmet:Predmet;
  rok: Ispit;
  idIspita:number;
  lista:Ucenik[]=[];
  ucenik:Ucenik;
  brojBodova:number=0;

  public selectedId:number;
  
  public highlightRow(emp: Ucenik) {
    this.selectedId = emp.id;
    this.ucenik = this.ucenikServis.getUcenik(this.selectedId);
    console.log(this.ucenik);
  }

  constructor(private predmetService:PredmetService,private route: ActivatedRoute, 
              private ucenikServis:UceniciService,private router: Router) { }

  ngOnInit() {

    this.route.params
    .subscribe(
      (params: Params) => {
        this.predmet = this.predmetService.getPredmet(+params['id']);
        this.idIspita = +params['idI'];
        this.predmetService.getPrijaveZaRok(this.idIspita).subscribe(
         ( data: Ispit) => {
              this.rok = data;
              for(let ucenik of this.rok.prijave){    
                const ucenik1: PrijavaUcenik = ucenik;        
               if(ucenik1.osvojeniBodovi == null){
                this.lista.push(this.ucenikServis.getUcenik(ucenik1.ucenik.id))
               }
             
           }
           
         }
          
          
        )
      }
    );
  }

  oceni(){
    this.predmetService.oceniUcenika(this.rok.id,this.ucenik.id,this.brojBodova).subscribe(
      data => {
      this.lista = this.lista.filter(item => item.id != this.ucenik.id),
      this.ucenik=null,
      this.brojBodova=null
      }
    )
    
    
  }

}
