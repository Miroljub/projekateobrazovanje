import { Component, OnInit } from '@angular/core';
import { Ucenik } from 'src/app/ucenici/ucenici.model';
import { PredmetService } from 'src/app/predmeti/predmet.service';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { UceniciService } from 'src/app/ucenici/ucenici.service';
import { Predmet } from 'src/app/predmeti/predmet.model';
import { Ocenjivanje } from 'src/app/models/ocenjivanje';

@Component({
  selector: 'app-davanje-ocene',
  templateUrl: './davanje-ocene.component.html',
  styleUrls: ['./davanje-ocene.component.css']
})
export class DavanjeOceneComponent implements OnInit {
  provera:boolean = true;
  ucenik:Ucenik;
  predmet:Predmet;
  ispiti: Ocenjivanje[]=[];
  osvojeniBodovi:number=0;
  konacnaOcena:number;
  searchText:string;
  ocene:number[]=[6,7,8,9,10]
  constructor(private predmetService:PredmetService,private route: ActivatedRoute,
              private ucenikServis:UceniciService, private router: Router) { 
                
              }


  ngOnInit() {
    this.route.params
    .subscribe(
      (params: Params) => {
        this.ucenik = this.ucenikServis.getUcenik(+params['idU']);
        this.predmet = this.predmetService.getPredmet(+params['id'])
      }
    );  
    this.predmetService.getIspite2(this.predmet.id,this.ucenik.id).subscribe(
      (ispiti: Ocenjivanje[]) => {
        this.ispiti = ispiti;
        for(let b of ispiti){
          if((+b.maksimalniBodovi * 0.51) < +b.osvojeniBodovi){
            this.osvojeniBodovi += +b.osvojeniBodovi;
          }
        }
      }
    )

    
    
  }

  oceni(){
    if(this.konacnaOcena > 5 || this.konacnaOcena <= 10){
      this.provera = true;
      this.predmetService.oceni(this.predmet.id,this.ucenik.id,this.konacnaOcena).subscribe(
        data => {
          this.router.navigate([`../nastavnik_predmeti/${this.predmet.id}/ucenici`])
        }
      );
     
      
    }
    else{
      this.provera = false;
    }
  }

}
